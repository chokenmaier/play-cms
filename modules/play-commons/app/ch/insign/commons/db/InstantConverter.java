/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.commons.db;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Timestamp;
import java.time.Instant;

/**
 * Converter for the {@link Instant} datatype. It converts an Instant to a {@link Timestamp}
 * to save it in the database efficiently and well readable.
 * <br />
 * <br />
 * This converter must be enabled in <code>persistence.xml</code> like this:
 * <code>
 *     <persistence-unit name="defaultPersistenceUnit" transaction-type="RESOURCE_LOCAL">
 *         <class>ch.insign.commons.db.InstantConverter</class>
 *     </persistence-unit>
 * </code>
 */
@Converter(autoApply = true)
public class InstantConverter implements AttributeConverter<Instant, Timestamp> {

    @Override
    public Timestamp convertToDatabaseColumn(Instant attribute) {
        if(attribute == null) {
            return null;
        } else {
            return Timestamp.from(attribute);
        }
    }

    @Override
    public Instant convertToEntityAttribute(Timestamp dbData) {
        if(dbData == null) {
            return null;
        } else {
            return dbData.toInstant();
        }
    }

}
