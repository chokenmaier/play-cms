/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.party;

import ch.insign.playauth.party.preference.Preference;
import org.apache.shiro.subject.PrincipalCollection;

import javax.annotation.Nonnull;
import java.util.Optional;
import java.util.Set;

public interface Party {

	String getId();

    String getName();

	String getEmail();

	PrincipalCollection getPrincipals();

	Object getCredentials();

	boolean isLocked();

	void setLocked(boolean locked);

    @Nonnull
    Set<PartyRole> getRoles();

    void addRole(@Nonnull PartyRole r);

    void removeRole(@Nonnull PartyRole r);

    boolean hasRole(@Nonnull PartyRole r);

    /**
     * Get a set of available party preferences
     */
    @Nonnull
    Set<Preference> getPreferences();

    /**
     * Optionally get the party's preference
     */
    @Nonnull
    Optional<Preference> getPreference(@Nonnull String name);

    /**
     * Set the party's preference
     */
    void setPreference(@Nonnull Preference p);

    /**
     * Optionally get the party's preference value
     */
    @Nonnull
    Optional<String> getPreferredOption(@Nonnull String name);

    /**
     * Set the party's most preferred option
     */
    void setPreferredOption(@Nonnull String name, @Nonnull String option);

    /**
     * Remove the party's preference and return the removed value
     */
    @Nonnull
	Optional<Preference> removePreference(@Nonnull String name);
}