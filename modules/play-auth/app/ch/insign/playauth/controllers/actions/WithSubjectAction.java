/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.controllers.actions;

import ch.insign.playauth.PlayAuthApi;
import ch.insign.playauth.authz.AuthorizationHandler;
import com.google.common.base.Throwables;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.UnauthenticatedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.mvc.Action;
import play.mvc.Http.Context;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class WithSubjectAction<T> extends Action<T>  {

	private static final Logger logger = LoggerFactory.getLogger(WithSubjectAction.class);

    private static final String WITH_SUBJECT_FLAG = "WITH_SUBJECT";

    @Inject
    private PlayAuthApi playAuthApi;

    @Inject
    private AuthorizationHandler authorizationHandler;

    protected PlayAuthApi getPlayAuthApi() {
        return playAuthApi;
    }

    protected AuthorizationHandler getAuthorizationHandler() {
        return authorizationHandler;
    }

    @Override
    public CompletionStage<Result> call(Context ctx) {
        // Skip thread state binding if the wrapped action is already aware of subject
        if (isWithSubject(ctx)) {
            return doCall(ctx);
        }

        try {
            // bind subject to current thread
            getPlayAuthApi().bind(ctx);
            ctx.args.put(WITH_SUBJECT_FLAG, true);

            try {
	            try {
		            return CompletableFuture.completedFuture(doCall(ctx).toCompletableFuture().get());
	            } catch (Throwable t) {
		            throw unwrapped(t);
	            }
            } catch (UnauthenticatedException e) {
                // Attempting to execute an authorization action when a
                // successful authentication hasn't yet occurred
                return CompletableFuture.completedFuture(getAuthorizationHandler().onUnauthenticated(ctx, e));
            } catch (AuthorizationException e) {
                // Access to a requested resource is not allowed
                return CompletableFuture.completedFuture(getAuthorizationHandler().onUnauthorized(ctx, e));
            } catch (Throwable t) {
                throw Throwables.propagate(t);
            }

        } finally {
            // unbind subject from current thread
            ctx.args.put(WITH_SUBJECT_FLAG, false);
            getPlayAuthApi().unbind();
        }
    }

    public CompletionStage<Result> doCall(Context ctx)  {
        return delegate.call(ctx);
    }

    private Boolean isWithSubject(Context ctx) {
        return ctx.args.containsKey(WITH_SUBJECT_FLAG) && (Boolean) ctx.args.get(WITH_SUBJECT_FLAG);
    }

	private static Throwable unwrapped(Throwable t) {
		if (!(t instanceof AuthorizationException)) {
			Throwable cause = Throwables.getRootCause(t);
			if (cause instanceof AuthorizationException) {
				return cause;
			}
		}
		return t;
	}
}
