/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.utils;

import com.google.common.base.Throwables;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.api.Application;
import play.api.Play;
import scala.Option;
import scala.runtime.AbstractFunction0;
import scala.runtime.AbstractFunction1;

import java.net.URL;
import java.util.Optional;
import java.util.function.Supplier;

public class CacheUtils {

	public static final String CONFIG_NAME = "ch.insign.playauth";

	private final static Logger logger = LoggerFactory.getLogger(CacheUtils.class);
	private static final String CONFIG_FILE_PATH = "ch.insign.playauth.ehcache.xml";

	public static CacheManager manager() {
		CacheManager manager = CacheManager.getCacheManager(CONFIG_NAME);
		if (manager == null) {
			URL configFilePath = Play.maybeApplication()
					.flatMap(new AbstractFunction1<Application, Option<URL>>() {
						@Override
						public Option<URL> apply(Application app) {
							return app.resource(CONFIG_FILE_PATH);
						}
					})
					.getOrElse(new AbstractFunction0<URL>() {
						@Override
						public URL apply() {
							return CacheUtils.class.getClassLoader().getResource(CONFIG_FILE_PATH);
						}
					});

			logger.debug("Initializing the cache manager \"{}\" with config at {}", CONFIG_NAME, configFilePath);
			manager = CacheManager.newInstance(configFilePath);
		}
		return manager;
	}

	public static Ehcache cache(String cacheName) {
		return manager().addCacheIfAbsent(cacheName);
	}

	@SuppressWarnings("unchecked")
	public static <T> T cached(String cacheName, Object key, Supplier<T> block) {
		Ehcache cache = cache(cacheName);

		return Optional.ofNullable(cache.get(key))
				.map(Element::getObjectValue)
				.flatMap(val -> {
					try {
						logger.trace("Accessing cached entry with key={}, value={}", key, val);
						return Optional.of((T) val);
					} catch (ClassCastException e) {
						return Optional.empty();
					}
				})
				.orElseGet(() -> {
					try {
						T val = block.get();
						cache.put(new Element(key, val));
						logger.trace("Added cache entry with key={}, value={}", key, val);
						return val;
					} catch (Throwable t) {
						throw Throwables.propagate(t);
					}
				});
	}

	public static void update(String cacheName, Object key, Object val) {
		logger.trace("Updating cache entry with key={}, value={}", key, val);
		cache(cacheName).put(new Element(key, val));
	}

	public static boolean remove(String cacheName, Object key) {
		logger.trace("Removing cache entry with key={}", key);
		return cache(cacheName).remove(key);
	}
}
