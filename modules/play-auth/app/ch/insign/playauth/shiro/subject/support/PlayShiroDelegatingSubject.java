/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.shiro.subject.support;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.SessionContext;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.support.DelegatingSubject;
import org.apache.shiro.util.StringUtils;

import play.mvc.Http;

public class PlayShiroDelegatingSubject extends DelegatingSubject implements ch.insign.playauth.shiro.subject.PlayShiroSubject {
	private final static Logger logger = LoggerFactory.getLogger(PlayShiroDelegatingSubject.class);

    private final Http.Context context;

    public PlayShiroDelegatingSubject(PrincipalCollection principals, boolean authenticated,
            String host, Session session, Http.Context context,
            SecurityManager securityManager) {
        this(principals, authenticated, host, session, true, context, securityManager);
    }

    public PlayShiroDelegatingSubject(PrincipalCollection principals, boolean authenticated,
            String host, Session session, boolean sessionEnabled,
            Http.Context context,
            SecurityManager securityManager) {
        super(principals, authenticated, host, session, sessionEnabled, securityManager);
        this.context = context;
    }

    @Override
    public Http.Context getHttpContext() {
        return context;
    }

    @Override
    protected SessionContext createSessionContext() {
        ch.insign.playauth.shiro.session.mgt.DefaultPlayShiroSessionContext sc = new ch.insign.playauth.shiro.session.mgt.DefaultPlayShiroSessionContext();
        String host = getHost();
        if (StringUtils.hasText(host)) {
            sc.setHost(host);
        }
        sc.setHttpContext(context);
        return sc;
    }
}
