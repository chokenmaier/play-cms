/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.authz;

import ch.insign.playauth.PlayAuth;
import jdk.nashorn.internal.ir.annotations.Immutable;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;
import static ch.insign.playauth.authz.AccessControlEntry.Utils.*;

/**
 * Represents an individual permission assignment.
 */
@IdClass(AccessControlEntry.Identifier.class)
@Entity
@Table(name = "auth_acl")
@NamedQueries(value = {
		@NamedQuery(
				name = "AccessControlEntry.countAll",
				query = "SELECT COUNT(ace) FROM AccessControlEntry ace"),
		@NamedQuery(
				name = "AccessControlEntry.findAll",
				query = "SELECT ace FROM AccessControlEntry ace"),
		@NamedQuery(
				name = "AccessControlEntry.findBySid",
				query = "SELECT ace FROM AccessControlEntry ace WHERE ace.sidType = :type AND ace.sidIdentifier = :identifier"),
		@NamedQuery(
				name = "AccessControlEntry.findByOid",
				query = "SELECT ace FROM AccessControlEntry ace WHERE ace.oidType = :type AND ace.oidIdentifier = :identifier")
})
public class AccessControlEntry {

	@Id
	@Column(length = 100)
	private String sidType;

	@Id
	@Column(length = 60)
	private String sidIdentifier;

	@Id
	@Column(length = 100)
	private String oidType;

	@Id
	@Column(length = 60)
	private String oidIdentifier;

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "auth_acl_allow")
	private Set<DomainActionEntry> allow = new HashSet<>();

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "auth_acl_deny")
	private Set<DomainActionEntry> deny = new HashSet<>();

	protected AccessControlEntry() {

	}

	public AccessControlEntry(SecurityIdentity sid, ObjectIdentity oid) {
		Objects.requireNonNull(sid);
		Objects.requireNonNull(oid);

		this.sidType = sid.getType();
		this.sidIdentifier = sid.getIdentifier();
		this.oidType = oid.getType();
		this.oidIdentifier = oid.getIdentifier();
	}

	public Identifier getId() {
		return new Identifier(getSid(), getOid());
	}

	public SecurityIdentity getSid() {
		return new SecurityIdentity(sidType, sidIdentifier);
	}

	public ObjectIdentity getOid() {
		return new ObjectIdentity(oidType, oidIdentifier);
	}

	public Set<DomainActionEntry> getAllow() {
		return allow;
	}

	public Set<DomainActionEntry> getDeny() {
		return deny;
	}

	/*
	public boolean allows(DomainActionEntry action) {
		return anyImplies(allow, getOid(), action);
	}

	public boolean allows(DomainPermission<?> p) {
		return anyImplies(allow, getOid(), p);
	}

	public boolean denies(DomainActionEntry action) {
		return anyImplies(deny, getOid(), action);
	}

	public boolean denies(DomainPermission<?> p) {
		return anyImplies(deny, getOid(), p);
	}

	public boolean implies(DomainActionEntry action) {
		return allows(action) || denies(action);
	}

	public boolean implies(DomainPermission<?> p) {
		return allows(p) || denies(p);
	}
	*/

	public boolean contains(DomainActionEntry action) {
		return allow.contains(action) || deny.contains(action);
	}

	public boolean contains(DomainPermission<?> p) {
		return contains(new DomainActionEntry(p));
	}

	public AccessControlEntry allow(DomainActionEntry action) {
		move(action, deny, allow);
		return this;
	}

	public AccessControlEntry allow(DomainPermission<?> p) {
		validate(p);
		return allow(new DomainActionEntry(p));
	}

	public AccessControlEntry deny(DomainActionEntry action) {
		move(action, allow, deny);
		return this;
	}

	public AccessControlEntry deny(DomainPermission<?> p) {
		validate(p);
		return deny(new DomainActionEntry(p));
	}

	public AccessControlEntry remove(DomainActionEntry action) {
		move(action, allow, new HashSet<>());
		move(action, deny, new HashSet<>());
		return this;
	}

	public AccessControlEntry remove(DomainPermission<?> p) {
		validate(p);
		return remove(new DomainActionEntry(p));
	}

	public Set<DomainPermission<?>> allowedPermissions() {
		return toDomainPermissions(getOid(), allow);
	}

	public Set<DomainPermission<?>> deniedPermissions() {
		return toDomainPermissions(getOid(), deny);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		AccessControlEntry that = (AccessControlEntry) o;

		if (!oidIdentifier.equals(that.oidIdentifier)) return false;
		if (!sidIdentifier.equals(that.sidIdentifier)) return false;
		if (!oidType.equals(that.oidType)) return false;
		if (!sidType.equals(that.sidType)) return false;
		if (!allow.equals(that.allow)) return false;
		if (!deny.equals(that.deny)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		return 37 * (getSid().hashCode() + getOid().hashCode());
	}

	private void validate(DomainPermission<?> p) {
		Validate.notNull(p);
		Validate.notBlank(p.domain());
		Validate.notBlank(p.name());
		Validate.isTrue(getOid().equals(p.target()),
				"Expected: \"" + getOid().getType() + ":" + getOid().getIdentifier() + "\"; " +
				"Actual: \"" + p.target().getType() + ":" + p.target().getIdentifier() + "\".");
	}


	/**
	 * A composite primary key for AccessControlEntry.
	 */
	@Immutable
	public static class Identifier implements Serializable {

		private static final long serialVersionUID = 1L;

		protected String sidType;
		protected String sidIdentifier;
		protected String oidType;
		protected String oidIdentifier;

		protected Identifier() {

		}

		public Identifier(SecurityIdentity sid, ObjectIdentity oid) {
			this.sidType = sid.getType();
			this.sidIdentifier = sid.getIdentifier();
			this.oidType = oid.getType();
			this.oidIdentifier = oid.getIdentifier();
		}

		public SecurityIdentity getSid() {
			return new SecurityIdentity(sidType, sidIdentifier);
		}

		public ObjectIdentity getOid() {
			return new ObjectIdentity(oidType, oidIdentifier);
		}

		@Override
		public String toString() {
			return new ToStringBuilder(this)
					.append("sid", getSid())
					.append("oid", getOid())
					.toString();
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;

			Identifier that = (Identifier) o;

			if (!oidIdentifier.equals(that.oidIdentifier)) return false;
			if (!sidIdentifier.equals(that.sidIdentifier)) return false;
			if (!oidType.equals(that.oidType)) return false;
			if (!sidType.equals(that.sidType)) return false;

			return true;
		}

		@Override
		public int hashCode() {
			int result = sidType.hashCode();
			result = 31 * result + sidIdentifier.hashCode();
			result = 31 * result + oidType.hashCode();
			result = 31 * result + oidIdentifier.hashCode();
			return result;
		}
	}

	/** Workaround for eclipselink [Bug 429992|https://bugs.eclipse.org/bugs/show_bug.cgi?id=429992] */
	protected final static class Utils {
		public static void move(DomainActionEntry action, Set<DomainActionEntry> from, Set<DomainActionEntry> to) {
			if (DomainActionEntry.ALL.equals(action)) {
				from.clear();
				to.clear();
			} else if (DomainActionEntry.ALL.name().equals(action.name())) {
				from.removeAll(filterByDomain(from, action.domain()));
				to.removeAll(filterByDomain(to, action.domain()));
			}

			from.remove(action);
			to.add(action);
		}

		public static Collection<DomainActionEntry> filterByDomain(Collection<DomainActionEntry> actions, String domain) {
			return actions.stream()
					.filter(r -> r.domain().equals(domain))
					.collect(Collectors.toSet());
		}

		public static Set<DomainPermission<?>> toDomainPermissions(ObjectIdentity oid, Set<DomainActionEntry> actions) {
			PermissionManager pm = PlayAuth.getPermissionManager();
			return actions.
					stream()
					.map(a -> pm.resolveDomainPermission(a.domain(), a.name(), oid))
					.collect(Collectors.toSet());
		}
	}
}