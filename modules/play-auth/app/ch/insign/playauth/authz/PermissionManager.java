/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.authz;

import java.util.Set;

/**
 * The central point to manage domain permisions.
 */
public interface PermissionManager {

	/**
	 * Adds a rule by which {@link AccessControlListVoter#vote} will return an
	 * allowing {@link AccessControlListVoter.Vote} for the given authority and
	 * the {@param right} permission if the given authority was granted the
	 * {@param left} permission.
	 *
	 * @param left The permission which if granted will also allow the {@param right} permission.
	 * @param right The permission which is implicitly allowed by the {@param left} permission.
     */
	void addAllowingRule(DomainPermission<?> left, DomainPermission<?> right);

	/**
	 * Resolves permissions which implicitly assume/allow the given permission.
     */
	Set<DomainPermission<?>> resolveAllowingPermissions(DomainPermission<?> permission);

	/**
	 * Resolves permissions which are implicitly assumed/allowed by the given permission.
     */
	Set<DomainPermission<?>> resolvePermissionsAllowedBy(DomainPermission<?> permission);

	/**
	 * Returns true if both permissions represent the same domain and the {@param left}
	 * permission is more general than the {@param right} permission.
     */
	boolean implies(DomainPermission<?> left, DomainPermission<?> right);

	/**
	 * Returns true if the corresponding allowing rule exists and the target of {@param right}
	 * permission is applicable to the {@param left} permission.
     */
	boolean allows(DomainPermission<?> left, DomainPermission<?> right);

	/**
	 * Adds a new permission {@param name} for the resources of type {@param domainType}.
     */
	<T> DomainPermission<T> definePermission(Class<T> domainType, String name);

	/**
	 * Returns all permissions defined by {@link this#definePermission}.
     */
	Set<DomainPermission<?>> getDefinedPermissions();

	/**
	 * Returns a set of {@link DomainPermission permissions} defined for the resources
	 * identified by the given {@link ObjectIdentity oid}.
	 */
	Set<DomainPermission<?>> getDefinedPermissions(ObjectIdentity oid);

	/**
	 * Returns a set of {@link DomainPermission<T> permissions} defined for the resources
	 * of type {@param targetType}.
     */
	<T> Set<DomainPermission<T>> getDefinedPermissions(Class<T> targetType);

	/**
	 * Returns a set of allowing permissions granted to the authority identified by the given
	 * {@link SecurityIdentity sid} on the resources identified by the given {@link ObjectIdentity oid}.
     */
	Set<DomainPermission<?>> getAllowedPermissions(SecurityIdentity sid, ObjectIdentity oid);

	/**
	 * Returns a set of allowing permissions granted to the authority identified by
	 * the given {@link SecurityIdentity sid}.
	 */
	Set<DomainPermission<?>> getAllowedPermissions(SecurityIdentity sid);

	/**
	 * Returns a set of denying permissions set to the authority identified by the given
	 * {@link SecurityIdentity sid} on the resources identified by the given {@link ObjectIdentity oid}.
	 */
	Set<DomainPermission<?>> getDeniedPermissions(SecurityIdentity sid, ObjectIdentity oid);

	/**
	 * Returns a set of denying permissions set to the authority identified by
	 * the given {@link SecurityIdentity sid}.
	 */
	Set<DomainPermission<?>> getDeniedPermissions(SecurityIdentity sid);

	/**
	 * Resolves the given arguments to the instance of {@link PermissionManager}
     */
	DomainPermission<?> resolveDomainPermission(String domain, String action, ObjectIdentity target);

	/**
	 * Returns a new DomainPermission with the given target applied.
	 */
	<T, V extends T> DomainPermission<T> applyTarget(DomainPermission<T> p, V target);

	/**
	 * Returns a new DomainPermission with the given target applied.
	 */
	<T, V extends T> DomainPermission<T> applyTarget(DomainPermission<T> p, Class<V> target);

	/**
	 * Returns a new DomainPermission with the given target applied.
	 */
	<T> DomainPermission<T> applyTarget(DomainPermission<T> p, ObjectIdentity target);

	/**
	 * Returns true if the given target can be applied to the given permission.
	 */
	boolean isTargetApplicable(DomainPermission<?> p, ObjectIdentity target);

	/**
	 * Returns the qualified name of the underlying DomainPermission.
	 *
	 * The qualified name is composed of the 'qualifier' and the 'local name' which are separated with dot (".").
	 */
	String getQualifiedName(DomainPermission<?> p);

	boolean allows(AccessControlEntry ace, DomainActionEntry action);

	boolean allows(AccessControlEntry ace, DomainPermission<?> p);

	boolean denies(AccessControlEntry ace, DomainActionEntry action);

	boolean denies(AccessControlEntry ace, DomainPermission<?> p);

	boolean implies(AccessControlEntry ace,DomainActionEntry action);

	boolean implies(AccessControlEntry ace, DomainPermission<?> p);
}
