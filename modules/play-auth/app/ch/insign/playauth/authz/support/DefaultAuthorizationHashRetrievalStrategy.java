/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.authz.support;

import ch.insign.playauth.authz.*;
import ch.insign.playauth.event.AuthorizationEvent;
import ch.insign.playauth.event.EventDispatcher;
import ch.insign.playauth.event.PermissionAuthorizationEvent;
import ch.insign.playauth.event.RoleAuthorizationEvent;
import ch.insign.playauth.party.Party;
import org.apache.shiro.crypto.hash.Sha1Hash;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

import static ch.insign.playauth.PlayAuth.getSecurityIdentity;

@Singleton
public class DefaultAuthorizationHashRetrievalStrategy implements AuthorizationHashRetrievalStrategy {

	private Map<SecurityIdentity, AuthorizationHash> cache = new ConcurrentHashMap<>();
	private final PermissionManager pm;

	@Inject
	public DefaultAuthorizationHashRetrievalStrategy(EventDispatcher dispatcher, PermissionManager pm) {
		this.pm = pm;

		dispatcher.addListener(AuthorizationEvent.class, event -> {
			if (event instanceof RoleAuthorizationEvent) {
				// if PartyRole is granted or revoked on a Party: clear the cache for this Party
				cache.remove(getSecurityIdentity(((RoleAuthorizationEvent) event).getParty()));
			} else if (event instanceof PermissionAuthorizationEvent) {
				SecurityIdentity sid = ((PermissionAuthorizationEvent) event).getSid();
				if (sid.getSource().filter(src -> src instanceof Party).isPresent()) {
					// if permission is allowed/denied/removed on a Party: clear the cache for this Party
					cache.remove(sid);
				} else {
					// clear all cache
					cache.clear();
				}
			}
		});
	}

	@Override
	public AuthorizationHash getAuthorizationHash(SecurityIdentity target) {
		Objects.requireNonNull(target);

		return cache.computeIfAbsent(target, key -> target
				.getSource()
				.flatMap(this::asAuthzPattern)
				.<AuthorizationHash>map(DefaultAuthorizationHash::new)
				.orElse(DefaultAuthorizationHash.EMPTY));
	}


	private Optional<String> asAuthzPattern(Object target) {
		return asSids(target)
				.stream()
				.<String>flatMap(sid -> Stream.concat(
						pm.getAllowedPermissions(sid).stream().map(this::asString).map(s -> "Y/" + s),
						pm.getDeniedPermissions(sid).stream().map(this::asString).map(s -> "N/" + s)))
				.distinct()
				.sorted()
				.reduce((a, b) -> a + ";" + b);

	}

	private Set<SecurityIdentity> asSids(Object target) {
		HashSet<SecurityIdentity> sids = new HashSet<>();

		// All
		sids.add(SecurityIdentity.ALL);

		// Party or PartyRole
		sids.add(getSecurityIdentity(target));

		// PartyRoles if target is Party
		if (target instanceof Party) {
			((Party) target).getRoles().forEach(r -> sids.add(getSecurityIdentity(r)));
		}

		return sids;
	}

	private String asString(DomainPermission<?> p) {
		return p.domain() + "/" + p.name() + "/" + p.target().getType() + "/" + p.target().getIdentifier();
	}


	private static class DefaultAuthorizationHash extends Sha1Hash implements AuthorizationHash {
		public static final AuthorizationHash EMPTY = new DefaultAuthorizationHash("");

		private DefaultAuthorizationHash(Object source) {
			super(source);
		}
	}
}
