/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.authz.support;

import ch.insign.playauth.authz.AuthorizationHandler;
import org.apache.shiro.authz.AuthorizationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.mvc.Http.Context;
import play.mvc.Result;
import play.mvc.Results;

public class DefaultAuthorizationHandler implements AuthorizationHandler {
	private final static Logger logger = LoggerFactory.getLogger(DefaultAuthorizationHandler.class);

    @Override
    public Result onUnauthorized(Context ctx, AuthorizationException e) {
        return Results.forbidden(views.html.defaultpages.unauthorized.render());
    }

    @Override
    public Result onUnauthenticated(Context ctx, AuthorizationException e) {
        return Results.unauthorized(views.html.defaultpages.unauthorized.render());
    }

}
