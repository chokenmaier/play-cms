/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.authz;

import ch.insign.playauth.event.EventDispatcher;
import ch.insign.playauth.event.PermissionAllowEvent;
import ch.insign.playauth.event.PermissionDenyEvent;
import ch.insign.playauth.event.PermissionRemoveEvent;
import ch.insign.playauth.permissions.GlobalDomainPermission;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.UnauthorizedException;

import javax.inject.Inject;
import java.util.Optional;

import static ch.insign.playauth.authz.DomainPermissionUtils.resolveDomainClass;

public class AccessControlManager {

	private final EventDispatcher dispatcher;
	private final PermissionManager pm;
	private final AccessControlList acl;
	private final AccessControlListVoter aclVoter;
	private final ObjectIdentityRetrievalStrategy oidrs;
	private final SecurityIdentityRetrievalStrategy sidrs;
	private final Authorizer authorizer;

	@Inject
	public AccessControlManager(EventDispatcher dispatcher,
								PermissionManager pm,
								AccessControlList acl,
								AccessControlListVoter aclVoter,
								ObjectIdentityRetrievalStrategy oidrs,
								SecurityIdentityRetrievalStrategy sidrs,
								Authorizer authorizer
	) {
		this.dispatcher = dispatcher;
		this.pm = pm;
		this.acl = acl;
		this.aclVoter = aclVoter;
		this.oidrs = oidrs;
		this.sidrs = sidrs;
		this.authorizer = authorizer;
	}

	public boolean isPermitted(Object authority, DomainPermission<?> permission) {
		return permission.target()
				// use a target object's Authorizer if available
				.getMetadata(Authorizer.class)
				// otherwise use a domain class's Authorizer if available
				.orElseGet(() -> resolveDomainClass(permission)
						.map(oidrs::getObjectIdentity)
						.flatMap(oid -> oid.getMetadata(Authorizer.class))
						// otherwise fall back to a default authorizer
						.orElseGet(() -> authorizer))
				.isPermitted(authority, permission);
    }

	public <T, V extends T> boolean isPermitted(Object authority, DomainPermission<T> permission, V target) {
		return isPermitted(authority, pm.applyTarget(permission, target));
	}

	public <T, V extends T> boolean isPermitted(Object authority, DomainPermission<T> permission, Class<V> target) {
		return isPermitted(authority, pm.applyTarget(permission, target));
	}

	public boolean isNotPermitted(Object authority, DomainPermission<?> permission) {
		return !isPermitted(authority, permission);
	}

	public <T, V extends T> boolean isNotPermitted(Object authority, DomainPermission<T> permission, V target) {
		return !isPermitted(authority, permission, target);
	}

	public <T, V extends T> boolean isNotPermitted(Object authority, DomainPermission<T> permission, Class<V> target) {
		return !isPermitted(authority, permission, target);
	}

	/**
	 * Tells whether authority will be allowed the given permission after
	 * removing {@link AccessControlManager#removePermission} the permission from ACE record.
	 */
	public boolean isPermittedByDefault(Object authority, DomainPermission<?> permission) {
		return aclVoter
				.inheritedVote(sidrs.getSecurityIdentity(authority), permission)
				.map(AccessControlListVoter.Vote::allowed)
				.orElse(false);
	}

	public boolean isPermittedEverything(Object authority) {
		return authorizer.isPermitted(authority, GlobalDomainPermission.ALL);
	}

	public void requirePermission(Object authority, DomainPermission<?> permission)  throws AuthorizationException {
		if (isNotPermitted(authority, permission)) {
			throw new UnauthorizedException(String.format("%s is not allowed to %s%s",
							// subject
							Optional.of(sidrs.getSecurityIdentity(authority))
									.filter(sid -> !sid.equals(SecurityIdentity.ALL))
									.map(sid -> sid.getType() + "(" + sid.getIdentifier() + ")")
									.orElse("Anonymous"),
							// verb
							pm.getQualifiedName(permission),
							// object
							Optional.of(permission.target())
									.filter(oid -> !oid.equals(ObjectIdentity.ALL))
									.map(oid -> " the target " + oid.getType() + "(" + oid.getIdentifier() + ")")
									.orElse("")
			));
		}
	}

	public <T, V extends T> void requirePermission(Object authority, DomainPermission<T> permission, V target)  throws AuthorizationException {
		requirePermission(authority, pm.applyTarget(permission, target));
	}

	public <T, V extends T> void requirePermission(Object authority, DomainPermission<T> permission, Class<V> target)  throws AuthorizationException {
		requirePermission(authority, pm.applyTarget(permission, target));
	}

	public void allowPermission(Object authority, DomainPermission<?> permission) {
		if (isPermittedEverything(authority)) {
			// Skipping: superuser is already allowed everything
			return;
		}

        SecurityIdentity sid = sidrs.getSecurityIdentity(authority);
		ObjectIdentity oid = permission.target();

		AccessControlEntry ace = acl.find(sid, oid)
				.orElse(new AccessControlEntry(sid, oid))
				.allow(permission);

		acl.put(ace);

		dispatcher.dispatch(new PermissionAllowEvent(sid, permission));
    }

	public <T, V extends T> void allowPermission(Object authority, DomainPermission<T> permission, V target) {
		allowPermission(authority, pm.applyTarget(permission, target));
	}

	public <T, V extends T> void allowPermission(Object authority, DomainPermission<T> permission, Class<V> target) {
		allowPermission(authority, pm.applyTarget(permission, target));
	}

    public void denyPermission(Object authority, DomainPermission<?> permission) {

	    if (isPermittedEverything(authority)) {
		    // Skipping: superuser should be allowed everything
		    return;
	    }

	    SecurityIdentity sid = sidrs.getSecurityIdentity(authority);
	    ObjectIdentity oid = permission.target();

	    AccessControlEntry ace = acl.find(sid, oid)
			    .orElse(new AccessControlEntry(sid, oid))
			    .deny(permission);

	    acl.put(ace);

	    dispatcher.dispatch(new PermissionDenyEvent(sid, permission));
    }

	public <T, V extends T> void denyPermission(Object authority, DomainPermission<T> permission, V target) {
		denyPermission(authority, pm.applyTarget(permission, target));
	}

	public <T, V extends T> void denyPermission(Object authority, DomainPermission<T> permission, Class<V> target) {
		denyPermission(authority, pm.applyTarget(permission, target));
	}

	public boolean containsPermission(Object authority, DomainPermission<?> permission) {
		return acl.find(sidrs.getSecurityIdentity(authority), permission.target())
				.filter(ace -> ace.contains(permission))
				.isPresent();
	}

	public <T, V extends T> boolean containsPermission(Object authority, DomainPermission<T> permission, V target) {
		return containsPermission(authority, pm.applyTarget(permission, target));
	}

	public <T, V extends T> boolean containsPermission(Object authority, DomainPermission<T> permission, Class<V> target) {
		return containsPermission(authority, pm.applyTarget(permission, target));
	}

	public boolean containsImpliedPermission(Object authority, DomainPermission<?> permission) {
		return acl.find(sidrs.getSecurityIdentity(authority), permission.target())
				.filter(ace -> pm.implies(ace, permission))
				.isPresent();
	}

	public <T, V extends T> boolean containsImpliedPermission(Object authority, DomainPermission<T> permission, V target) {
		return containsPermission(authority, pm.applyTarget(permission, target));
	}

	public <T, V extends T> boolean containsImpliedPermission(Object authority, DomainPermission<T> permission, Class<V> target) {
		return containsPermission(authority, pm.applyTarget(permission, target));
	}

	public void removePermission(Object authority, DomainPermission<?> permission) {
		SecurityIdentity sid = sidrs.getSecurityIdentity(authority);

		acl.find(sid, permission.target())
				.ifPresent(ace -> {
                    ace.remove(permission);
                    acl.put(ace);
                });

		dispatcher.dispatch(new PermissionRemoveEvent(sid, permission));
	}

	public <T, V extends T> void removePermission(Object authority, DomainPermission<T> permission, V target) {
		removePermission(authority, pm.applyTarget(permission, target));
	}

	public <T, V extends T> void removePermission(Object authority, DomainPermission<T> permission, Class<V> target) {
		removePermission(authority, pm.applyTarget(permission, target));
	}

	public DomainPermission<?> clearMetadata(DomainPermission<?> p) {
		ObjectIdentity target = new ObjectIdentity(p.target().getType(), p.target().getIdentifier());
		return pm.applyTarget(p, target);
	}
}
