(function($) {
    $(document).on('submit', '#loginModal form', function() {
        var form = $(this);
        $('div.login-error').addClass("hide");
        $('div.group-email').removeClass("has-error");
        $('.group-email .help-block').addClass("hide").html("");
        $('div.group-password').removeClass("has-error");
        $('.group-password .help-block').addClass("hide").html("");
        form.ajaxSubmit({
            dataType : 'json',
            success : function(response) {
                if (response.status === 'redirect') {
                    window.location.href = response.content;
                } else if (response.status === 'error') {
                    $('input[name=password]').val("");
                    if(response.errors) {
                        for(index in response.errors) {
                            if (index === "") {
                                $('div.login-error')
                                    .html(response.errors[index].join("<br />"))
                                    .removeClass("hide");
                            } else {
                                $("div.group-"+index).addClass("has-error");
                                $(".group-"+index+" .help-block.errors").html(response.errors[index].join("<br />"))
                                    .removeClass("hide");
                            }
                        }
                    }
                } else {
                    window.location.reload(true);
                }
            }
        });
        return false;
    });
})(jQuery);
