jQuery(document).ready(function ($) {

    var prefixes = ["", ".de", ".en", ".fr", ".it", ".es"];

    prefixes.forEach(function (prefix) {
        initUpdateVpath(prefix);
        initAddToVpathHistory(prefix);
    });

    /**
     * Triggers when the vpath field is empty
     * and the page title field was filled in (for cms and product pages...)
     * @param prefix
     */
    function initUpdateVpath(prefix) {
        var timeout = null;
        var $xhr;
        var $vpath = $("input[name='vpath" + prefix + "']");
        var vpathExists = $vpath.val() != "";

        $("input[name='pageTitle" + prefix + "']").keyup(function () {

            var text = $(this).val().trim();

            if (vpathExists && $vpath.val().length > 0) {
                return;
            }

            if (timeout) {
                clearTimeout(timeout);
            }

            if ($xhr) {
                $xhr.abort();
            }

            $vpath.prop('disabled', true);

            timeout = setTimeout(function () {
                if (text.length === 0) {
                    $vpath.val("");
                    $vpath.prop('disabled', false);
                    return;
                }

                $xhr = $.getJSON($vpath.data("url"), {title: text, "nav-item-id": $vpath.data("nav-item-id") }, function (data) {
                    $vpath.prop('disabled', false);
                    $vpath.val(data.vpath);
                    $vpath.trigger('change');
                });

            }, 1000);

        });
    }

    /**
     * If the user tries to change an existing vpath (=object was already saved with that vpath),
     * then the checkbox "301-Redirect für den bisherigen Pfad anlegen" should be automatically selected.
     * If the admin deselects the checkbox no 301 should be saved.
     * @param prefix
     */
    function initAddToVpathHistory(prefix) {
        var $vpath = $("input[name='vpath" + prefix + "']");
        var $addToVpathHistory = $("input[name='addToVpathHistory" + prefix + "']");

        var vpathOrigValue = $vpath.val();
        var vpathPrevValue = $vpath.val();
        // no make sense for new item
        if (vpathOrigValue === "") {
            return;
        }

        $vpath.keyup(function () {

            var vpathVal = $(this).val();

            if (vpathVal !== vpathOrigValue) {
                if (vpathVal !== vpathPrevValue) {
                    $addToVpathHistory.prop('checked', true);
                }
            } else {
                $addToVpathHistory.prop('checked', false);
            }

            $.uniform.update($addToVpathHistory);

            vpathPrevValue = vpathVal;
        }).change(function () {

            var vpathVal = $(this).val();

            if (vpathVal !== vpathOrigValue) {
                if (vpathVal !== vpathPrevValue) {
                    $addToVpathHistory.prop('checked', true);
                }
            } else {
                $addToVpathHistory.prop('checked', false);
            }

            $.uniform.update($addToVpathHistory);

            vpathPrevValue = vpathVal;
        });

    }
});
