@()

@import ch.insign.commons.i18n.Language
@import ch.insign.cms.models.CMS
@import ch.insign.cms.models.Template
@import ch.insign.cms.controllers.CmsContext
@import ch.insign.cms.views.html.tags._asset
@import ch.insign.cms.models.FrontendWidget

@*
    // This file specifies global javascript properties for libraries used in the cms-application backend (admin-area of cms) as well as frontend (web-application specfic)
    // This configuration can be extended or overwritten on web-application level by editing controllers.views.js._config
*@

@mstringLanguage() = @{
    if(CMS.getConfig.frontendLanguages.contains(Language.getCurrentLanguage)) {
        Language.getCurrentLanguage
    } else {
        CMS.getConfig.frontendLanguages.get(0)
    }
}

@*
    // Optional settings configurable in cms-config (referenced from jsconfig object below
*@
var dynamics = {
    "tinyMce" : {
        optionalPlugins: {
            jbimages: {
                identifier: @if(CMS.getConfig.isImageUploadEnable){"jbimages"}else{ @if(CMS.getConfig.isResponsiveFileManagerEnable) {"responsivefilemanager"} else {""} },
                toolbar1: @if(CMS.getConfig.isMoxieManagerEnable){" link image insertfile "}else{""},
                toolbar2: @if(CMS.getConfig.isImageUploadEnable){"jbimages | "}else{ @if(CMS.getConfig.isResponsiveFileManagerEnable) {"responsivefilemanager |"} else {""} }
            }
        }
    }
};


@*
// Global jsConfiguration
*@
jsconfig = {
    cms: {
        currentLanguage: "@Language.getCurrentLanguage",
        mstringLanguage: "@mstringLanguage"
    },

    wysihtml5: {
        tableAdvancedStylesheets: '@{_asset("plugins/bootstrap-wysihtml5/wysiwyg-color.css")}'
    },

    datatables:  {
        //default layout without horizontal scroll(remove this setting to enable horizontal scroll for the table)
        sDom: "<'row'<'clearfix'<'col-md-6 col-sm-12 input_search'><'col-md-6 col-sm-12'l>><'text-center'r>><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
        aLengthMenu: [
            [20, 50, 100, -1],
            [20, 50, 100, '@Messages("backend.datatables.navigation.all")']
        ],
        bProcessing: true,
        bServerSide: true,
        iDisplayLength: 100,
        sPaginationType: "bootstrap",
        oLanguage: {
            sProcessing: '<i class="fa fa-coffee"></i>&nbsp;@Messages("backend.datatables.pleaseWait")',
            sSearch:'',
            sLengthMenu: "_MENU_",
            oPaginate: {
                sPrevious: '@Messages("backend.datatables.search.navigation.back")',
                sNext: '@Messages("backend.datatables.search.navigation.forward")'
            },
            sInfo: '@Messages("backend.datatables.search.navigation.fromTo")',
            sInfoEmpty: '@Messages("backend.datatables.search.noEntries")',
            sInfoFiltered: '@Messages("backend.datatables.search.filtered")',
            sEmptyTable: '@Messages("backend.datatables.search.empty")',
            sZeroRecords: '<br><i class="fa fa-search text-muted"></i>@Messages("backend.datatables.search.noEntries")<br><br>'
        }
    },

    tinyMce: {
        defaultLang: @if(Language.getCurrentLanguage.equals("de")){"de"}else{"en"},
        simpleLayout: {
            plugins: [ "compat3x", "link",  "code", "paste", "textcolor", "advlist", "image", "codesample", dynamics.tinyMce.optionalPlugins.jbimages.identifier],
            toolbar: "undo redo | styleselect | bold italic | " + dynamics.tinyMce.optionalPlugins.jbimages.toolbar1 + " | code | bullist | forecolor backcolor | codesample"
        },
        defaultLayout: {
            plugins: [
                "compat3x", "advlist", "autolink", "lists", "link", "image", "charmap", "preview",  "anchor",
                "searchreplace", "wordcount", "visualblocks", "code", "fullscreen",
                "media", "nonbreaking", "save", "table", "contextmenu", "directionality", "codesample",
                "emoticons", "template", "paste", "textcolor", dynamics.tinyMce.optionalPlugins.jbimages.identifier
            ],
            toolbar1: "template undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | " + dynamics.tinyMce.optionalPlugins.jbimages.toolbar1,
            toolbar2: "preview media | forecolor backcolor | " + dynamics.tinyMce.optionalPlugins.jbimages.toolbar2 + " code codesample",
            table_default_attributes: {
                class: 'table table-bordered'
            },
            table_class_list: [
                { title: 'Bordered', value: 'table table-bordered' },
                { title: 'Condensed table', value: 'table table-condensed' },
                { title: 'Striped rows', value: 'table table-striped' },
                { title: 'Hover rows', value: 'table table-hover' },
                { title: 'Bordered and hover rows', value: 'table table-bordered table-hover' },
                { title: 'Bordered and striped rows', value: 'table table-bordered table-striped' },
                { title: 'Condensed and hover rows', value: 'table table-condensed table-hover' },
                { title: 'Condensed and striped rows', value: 'table table-condensed table-striped' }
            ],
            setup_table_responsive: function(editor) {
                editor.addButton('tableresponsive', {
                    tooltip: 'Responsive table',
                    icon: 'glyphicon-phone',
                    onclick: function() {
                        var ed = tinymce.activeEditor;
                        var dom = tinymce.activeEditor.dom;
                        var currentNode = null;

                        if (ed.selection.getNode().nodeName == "TABLE") {
                            currentNode = ed.selection.getNode().nodeName;
                        }
                        else {
                            var parents = dom.getParents( ed.selection.getNode() );
                            for( var i=0; i < parents.length; i++ )
                            {
                                currentNode = parents[i];
                                if( currentNode.nodeName == 'TABLE')
                                {
                                    break;
                                }
                            }
                        }

                        if (currentNode) {
                            jQuery(currentNode).wrap("<div class='table-responsive'></div>");
                        }
                    },
                });
            },
            table_toolbar: "tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | tableresponsive",
            external_filemanager_path: @if(CMS.getConfig.isResponsiveFileManagerEnable) {"@{CMS.getConfig.getResponsiveFileManagerUrlPrefix}filemanager/"} else {""}
        },
        "language_url": "@{ch.insign.commons.views.html.tags._asset("javascripts/tinymce_4.3.2/langs/" + Language.getCurrentLanguage + ".js")}",
        templates: [
            @for(filter <- FrontendWidget.getAllWidgets){
                {
                    title: "@filter._2.getClass.getSimpleName ",
                    content: "@{ "[[" + filter._1 + "]]" }",
                    description: "@Messages("widgets." + filter._2.getClass.getSimpleName + ".description")"
                },
            }
            {
                title: "Bild links, Text rechts",
                content: "",
                description: ""
            }
        ],
        externalPlugins: {
            compat3x: "@{ch.insign.commons.views.html.tags._asset("javascripts/tinymce_4.3.2/plugins/compat3x/plugin.js")}",

            @if(CMS.getConfig.isMoxieManagerEnable){
                moxiemanager: "/moxiemanager/plugin.js",
            }

            @if(CMS.getConfig.isResponsiveFileManagerEnable){
                responsivefilemanager: "@{CMS.getConfig.getResponsiveFileManagerUrlPrefix}tinymce/plugins/responsivefilemanager/plugin.min.js",
                filemanager: "@{CMS.getConfig.getResponsiveFileManagerUrlPrefix}filemanager/plugin.min.js",
            }

            @if(CMS.getConfig.isImageUploadEnable) {
                jbimages: "@{ch.insign.commons.views.html.tags._asset("javascripts/tinymce_4.3.2/plugins/jbimages/plugin.js")}"
            }
        }
    }
};
