/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms;

import akka.actor.ActorSystem;
import ch.insign.cms.blocks.backendlinkblock.BackendLinkBlock;
import ch.insign.cms.blocks.errorblock.ErrorPage;
import ch.insign.cms.blocks.groupingblock.GroupingBlock;
import ch.insign.cms.blocks.jotformpageblock.JotFormPageBlock;
import ch.insign.cms.blocks.jotformpageblock.actor.SyncFormSubmissions;
import ch.insign.cms.blocks.jotformpageblock.service.JotFormService;
import ch.insign.cms.blocks.linkblock.LinkBlock;
import ch.insign.cms.blocks.searchresultblock.SearchResultBlock;
import ch.insign.cms.blocks.sliderblock.SliderCollectionBlock;
import ch.insign.cms.models.*;
import ch.insign.commons.db.MString;
import ch.insign.commons.filter.TagContentFilter;
import ch.insign.commons.util.Configuration;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.inject.Singleton;

import static ch.insign.commons.db.DataBinding.registerDataBinder;


@Singleton
public class CMSApiLifecycleImpl implements CMSApiLifecycle {

    private final JPAApi jpaApi;

    private final ActorSystem actorSystem;

    @Inject
    public CMSApiLifecycleImpl(JPAApi jpaApi, ActorSystem actorSystem) {
        this.jpaApi = jpaApi;
        this.actorSystem = actorSystem;
    }

    @Override
    public void onStart(CMSApi cmsApi) {
        clearStaticState(cmsApi);

        // Register the cms blocks
        registerCmsBlocks(cmsApi);

        // Register content filters and add to filterable classes
        registerContentFilters(cmsApi);

        // Register default routes resolver
        registerResolver(cmsApi);

        // Register the default search provider
        registerSearchProviders(cmsApi);

        // Register uncached live partials
        registerUncachedPartials(cmsApi);

        // Register default email service
        registerEmailService(cmsApi);

        registerJotFormService(cmsApi);

        scheduleJobs(cmsApi);

        registerDataBinders();
    }

    @Override
    public void onStop(CMSApi cmsApi) {
        // nothing to do
    }

    protected void clearStaticState(CMSApi cmsApi) {
        // Flush internal caches
        BlockFinder.flush();
        MString.setFilterManager(cmsApi.getFilterManager());
    }

    protected void scheduleJobs(CMSApi cmsApi) {
        if (Configuration.getOrElse("jotform.api.akka.actor.SyncFormSubmissions.enabled", false)) {
            SyncFormSubmissions.start();
        }
    }

    protected void registerJotFormService(CMSApi cmsApi) {
        cmsApi.setJetFormSetup(new JotFormService());
    }

    protected void registerUncachedPartials(CMSApi cmsApi) {
    }

    protected void registerSearchProviders(CMSApi cmsApi) {
        // Use ElasticSearch as search provider if enabled
        if (cmsApi.getConfig().useElasticSearchProvider()) {
            cmsApi.getSearchManager().register(new ElasticSearchProvider(jpaApi, actorSystem));
        }
    }

    protected void registerResolver(CMSApi cmsApi) {
        cmsApi.setRouteResolver(new DefaultRouteResolver());
    }

    protected void registerEmailService(CMSApi cmsApi) {
        // not required anymore - do nothing
    }

    @SuppressWarnings("unchecked")
    protected void registerCmsBlocks(CMSApi cmsApi) {
        cmsApi.getBlockManager().register(
                PageBlock.class,
                ContentBlock.class,
                CollectionBlock.class,
                LinkBlock.class,
                BackendLinkBlock.class,
                GroupingBlock.class,
                SliderCollectionBlock.class,
                SearchResultBlock.class,
                JotFormPageBlock.class,
                ErrorPage.class);
    }

    protected void registerContentFilters(CMSApi cmsApi) {
        cmsApi.getFilterManager().register(
                new TagContentFilter(),
                new LinkContentFilter());
    }

    protected void registerDataBinders() {
        registerDataBinder(MString.class, new MStringDataBinder());
    }
}
