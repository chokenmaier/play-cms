/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms;

import ch.insign.cms.blocks.jotformpageblock.service.JotFormService;
import ch.insign.cms.email.EmailService;
import ch.insign.cms.models.*;
import ch.insign.commons.filter.FilterManager;
import ch.insign.commons.search.SearchManager;
import ch.insign.playauth.controllers.RouteResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.inject.ApplicationLifecycle;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.concurrent.CompletableFuture;

@Singleton
public class CMSApiImpl implements CMSApi {
    private final static Logger logger = LoggerFactory.getLogger(CMSApi.class);

    private CmsConfiguration configuration;
    private Sites sites = new Sites();
    private BlockManager blockManager;
    private FilterManager filterManager;
    private SearchManager searchManager;
    private UncachedManager uncachedManager;
    private RouteResolver routeResolver;
    private EmailService emailService;
    private JotFormService jotFormService;

    @Inject
    public CMSApiImpl(CMSApiLifecycle cmsApiLifecycle, ApplicationLifecycle lifecycle, EmailService emailService) {
        configuration = new CmsConfiguration();
        sites = new Sites();
        jotFormService = new JotFormService();
        blockManager = new BlockManager();
        filterManager = new FilterManager();
        searchManager = new SearchManager();
        uncachedManager = new UncachedManager();
        this.emailService = emailService;

        cmsApiLifecycle.onStart(this);

        lifecycle.addStopHook(
                () -> CompletableFuture.runAsync(
                        () -> cmsApiLifecycle.onStop(CMSApiImpl.this)));
    }

    @Override
    public CmsConfiguration getConfig() {
        return configuration;
    }

    /**
     * Get the sites configured in this project.
     */
    @Override
    public Sites getSites() {
        return sites;
    }

    @Override
    public JotFormService getJotFormService() {
        return jotFormService;
    }

    /**
     * Provide a project-specific configuration instance on app start.
     *
     * @param configInstance Project-specific implementation
     */
    @Override
    public void setConfig(CmsConfiguration configInstance) {
        logger.info("Setting cms configuration to: " + configInstance);
        configuration = configInstance;
    }

    @Override
    public void setRouteResolver(RouteResolver routeResolver) {
        this.routeResolver = routeResolver;
    }

    @Override
    public RouteResolver getRouteResolver() {
        return routeResolver;
    }

    @Override
    public BlockManager getBlockManager() {
        return blockManager;
    }

    @Override
    public FilterManager getFilterManager() {
        return filterManager;
    }

    @Override
    public SearchManager getSearchManager() {
        return searchManager;
    }

    @Override
    public UncachedManager getUncachedManager() {
        return uncachedManager;
    }

    /**
     * @deprecated The preferred method to obtain the EmailService is to inject it.
     */
    @Override
    @Deprecated
    public EmailService getEmailService() {
        return emailService;
    }

    /**
     * Provide a project-specific block manager on app start.
     * Note: This should generally not be necessary.
     *
     * @param blockManagerInstance Project-specific implementation
     */
    @Override
    public void setBlockManager(BlockManager blockManagerInstance) {
        logger.info("Setting cms blockManager to: " + blockManagerInstance);
        blockManager = blockManagerInstance;
    }

    @Override
    public void setJetFormSetup(JotFormService jotFormServiceInstance) {
        logger.info("Setting cms jot form setup instance to: " +  jotFormServiceInstance);
        jotFormService = jotFormServiceInstance;
    }

    /**
     * Provide a project-specific DefaultEmailService
     * Note: Most projects should customize default email layout.
     *
     * @param emailServiceInstance
     * @deprecated Registering the EmailService is not required anymore, the guice module will create it.
     */
    @Override
    @Deprecated
    public void registerEmailService(EmailService emailServiceInstance) {
        logger.info("Setting emailService instance to: " + emailServiceInstance);
        emailService = emailServiceInstance;
    }


}
