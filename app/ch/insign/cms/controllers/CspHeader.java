/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.controllers;

import ch.insign.cms.models.CMS;
import org.slf4j.LoggerFactory;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

import java.util.concurrent.CompletionStage;

/**
 * Add the Content Security Policy header
 * which will prevent the loading of non-approved external content.
 *
 * (https://developer.mozilla.org/en-US/docs/Web/Security/CSP/CSP_policy_directives)
 */
public class CspHeader extends Action<Void> {
	private final static org.slf4j.Logger logger = LoggerFactory.getLogger(CspHeader.class);

	public CompletionStage<Result> call(final Http.Context ctx) {
		if (CMS.getConfig().isCspEnabled()) {
			ctx.response().setHeader(
					"Content-Security-Policy",
					CMS.getConfig().cspResponseHeader() + " "
						+ CMS.getConfig().cspAdditionalResponseHeader().orElse("")
						+ "; report-uri /log/csp-violations"
			);
		}
		return delegate.call(ctx);
	}
}
