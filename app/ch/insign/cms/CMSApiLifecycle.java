/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms;

/**
 * This is used to hook into CMSApi lifecycle events.
 *
 * The onStart method is usually called by the CMSApi constructor.
 *
 * The onStop method is executed when the play application is shutdown.
 */
public interface CMSApiLifecycle {
    /**
     * Should be called when the CMSApi initializes.
     *
     * @param cmsApi
     */
    void onStart(CMSApi cmsApi);

    /**
     * Should be called when the play application stops.
     */
    void onStop(CMSApi cmsApi);
}
