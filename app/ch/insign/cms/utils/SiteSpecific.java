/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.utils;

import ch.insign.cms.models.Sites.Site;

import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * Class to do specific operations for one Site.
 * A new instance is created using <code>SiteSpecific.forSite</code>. Typically it's used together with
 * CMS.getSites().current() like this:
 *
 * <code>
 *  SiteSpecific
 *      .<Result>forSite(Sites.current(), Results::notFound)
 *      .onSiteKey(Sites.SITE_A.name(), () -> showSiteA())
 *      .onSiteKey(Sites.SITE_B.name(), () -> showSiteB())
 *      .get();
 * </code>
 *
 * This example will return Results.notFound() by default and the specific result for SITE_A and SITE_B.
 *
 * The API of the class is designed to be chained.
 *
 * You can use this class with any result type (T), thus, it could also be used
 * to render one template on one site, and another on another site.
 *
 * You can chain many operations together, to make special cases for various situations. In general,
 * <code>on(sitePredicate, specialCase)</code> can be used to create a special case:
 *
 * <code>
 *     on(site -> {
 *         // custom predicate to check if this special case applies for this site
 *         return true;
 *     }, () -> {
 *         // custom code to run, if the predicate matches
 *         return xyz;
 *     })
 * </code>
 *
 * There are some helper methods which simplify this:
 *
 * <ul>
 *     <li>
 *         <code>onSiteKey(siteKey, specialCase)</code> To match the key of the Site (case-insensitive)
 *     </li>
 *     <li>
 *         <code>onSiteName(siteName, specialCase)</code> To match the name of the Site (case-insensitive)
 *     </li>
 * </ul>
 *
 * @param <T> The type
 */
public final class SiteSpecific<T>  {

    private final Site site;

    private final Supplier<T> fallback;

    private SiteSpecific(Site site, Supplier<T> fallback) {
        this.site = site;
        this.fallback = fallback;
    }

    /**
     * Returns the result
     * This method checks whether specific operations should be applied (recursively).
     * If there are none, it will return the fallback.
     */
    public T get() {
        return fallback.get();
    }

    /**
     * Adds specific code for a site.
     * This generic method allows for any check against the site.
     */
    public SiteSpecific<T> on(Predicate<Site> sitePredicate, Supplier<T> specialCase) {
        return new SiteSpecific<T>(site, () -> {
            if(sitePredicate.test(site)) {
                return specialCase.get();
            } else {
                return fallback.get();
            }
        });
    }

    /**
     * Adds specific code for a site with a certain name.
     * The code is run when the key matches the hostname (case-insensitive).
     */
    public SiteSpecific<T> onSiteKey(String siteKey, Supplier<T> specialCase) {
        return on(site -> site.key.equalsIgnoreCase(siteKey), specialCase);
    }

    /**
     * Adds specific code for a site with a certain name.
     * The code is run when the given name matches the site's name (case-insensitive).
     */
    public SiteSpecific<T> onSiteName(String siteName, Supplier<T> specialCase) {
        return on(site -> site.name.equalsIgnoreCase(siteName), specialCase);
    }

    /**
     * Adds specific code for a given host.
     * The code is run when the given host matches any of the site's hostnames (case-insensitive).
     */
    public SiteSpecific<T> onSiteHost(String siteHost, Supplier<T> specialCase) {
        return on(site -> isSiteHost(site, siteHost), specialCase);
    }

    /** Creates a new specific operation for a given Site, with a fallback. */
    public static <T> SiteSpecific<T> forSite(Site site, Supplier<T> fallback) {
        return new SiteSpecific<T>(site, fallback);
    }

    /** Checks if the name of the site or any of it's hosts match the given host */
    private static boolean isSiteHost(Site site, String host) {
        return site.name.equalsIgnoreCase(host) ||
                site.hosts.stream()
                        .filter(siteHost -> siteHost.equalsIgnoreCase(host))
                        .count() > 0;
    }

}