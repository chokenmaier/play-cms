/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models.partyrole.view.support;

import ch.insign.cms.models.partyrole.view.PartyRoleListView;
import ch.insign.cms.views.admin.utils.AdminContext;
import ch.insign.cms.views.html.admin.role.list;
import ch.insign.playauth.party.PartyRoleManager;
import play.twirl.api.Html;

import javax.inject.Inject;
import java.util.ArrayList;

public class DefaultPartyRoleListView implements PartyRoleListView {

    private PartyRoleManager pm;

    @Inject
    public DefaultPartyRoleListView(PartyRoleManager pm) {
        this.pm = pm;
    }

    @Override
    public Html render() {
        return list.render(new AdminContext(), new ArrayList<>(pm.findAll()));
    }
}
