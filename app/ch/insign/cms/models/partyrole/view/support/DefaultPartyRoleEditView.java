/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models.partyrole.view.support;

import ch.insign.cms.models.partyrole.view.PartyRoleEditView;
import ch.insign.cms.views.admin.utils.AdminContext;
import ch.insign.playauth.PlayAuthApi;
import ch.insign.playauth.party.PartyRole;
import play.data.Form;
import play.twirl.api.Html;

import javax.inject.Inject;

public class DefaultPartyRoleEditView implements PartyRoleEditView {
    protected PartyRole role;
    protected Form<? extends PartyRole> editForm;
    protected PlayAuthApi authApi;

    @Inject
    public DefaultPartyRoleEditView(PlayAuthApi authApi) {
        this.authApi = authApi;
    }

    @Override
    public PartyRoleEditView setPartyRole(PartyRole role) {
        this.role = role;
        return this;
    }

    @Override
    public PartyRoleEditView setForm(Form<? extends PartyRole> editForm) {
        this.editForm = editForm;
        return this;
    }

    @Override
    public Html render() {
        return ch.insign.cms.views.html.admin.role.edit.render(
                new AdminContext(), role.getId(), editForm,
                ch.insign.cms.views.html.admin.role._permissions_checklist.render(authApi, editForm)
        );
    }
}
