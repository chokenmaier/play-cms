/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;

import com.typesafe.config.ConfigException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import ch.insign.commons.util.Configuration;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Managed CMS config values from application.conf
 */
public class CmsConfiguration extends Configuration  {
	private final static Logger logger = LoggerFactory.getLogger(CmsConfiguration.class);

    private static final int DEFAULT_SMTP_PORT = 25;

    @Override
    protected String configNamespace() {
        return "cms";
    }

    @Override
    protected void test() {
        baseUrl();
        backendTitle();
        deleteWithoutTrashBin();
        isResetRouteEnabled();
        defaultUrlPrefix();
        vpathTemplate();
        imageUploadRootPath();
        imageUploadWWWRoot();
        isImageUploadEnable();
        isResponsiveFileManagerEnable();
        getResponsiveFileManagerPhysicalRoot();
        getResponsiveFileManagerUrlPrefix();
        additionalScriptUrls();
        additionalStyleUrls();
        isMoxieManagerEnable();
        useElasticSearchProvider();
        backendPath();
        isCspEnabled();
        cspResponseHeader();
        getReviewSubmissionsCount();
    }

    /**
     * The base url of the website. Default: http://localhost:9000
     */
    public String baseUrl() {
        return getConfig().getString("baseUrl");
    }

    /**
     * The backend title as shown in the header
     */
    public String backendTitle() {
        return getConfig().getString("backend.title");
    }

    public String backendPath() {
        return getConfig().getString("backend.path");
    }

    public List<String> getSharedRoutes() {
        return getConfig().getStringList("shared.routes");
    }

    public boolean deleteWithoutTrashBin() {
        return getConfig().getBoolean("deleteWithoutTrashBin");
    }

    /**
     * Allow /reset to be called to reset everything.
     */
    public boolean isResetRouteEnabled() {
        return getConfig().getBoolean("enableResetRoute");
    }

    public List<Class> getAttributeClasses(){
        List<String> list = getConfig().getStringList("attributes.registered");
        List<Class> res = new ArrayList<>();
        for (String str: list){
            try {
                res.add(Class.forName(str));
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    /**
     * The url to use if no virtual path was set for a page.
     * Default is /page/<id>
     */
    public String defaultUrlPrefix() {
        return getConfig().getString("defaultUrlPrefix");
    }

    public String vpathTemplate() {
        return getConfig().getString("vpathTemplate");
    }

    public String imageUploadRootPath() {
        return getConfig().getString("imageupload.rootpath");
    }

    public String imageUploadWWWRoot() {
        return getConfig().getString("imageupload.wwwroot");
    }

    public boolean isImageUploadEnable() {
        return getConfig().getBoolean("imageupload.enable");
    }

    public boolean isMoxieManagerEnable() {
        return getConfig().getBoolean("moxiemanager.enable");
    }

    public boolean isResponsiveFileManagerEnable() {
        return getConfig().getBoolean("filemanager.enable");
    }

    public String getResponsiveFileManagerPhysicalRoot() {
        return getConfig().getString("filemanager.physicalroot");
    }

    public String getResponsiveFileManagerUrlPrefix() {
        return getConfig().getString("filemanager.urlPrefix");
    }

    public boolean useElasticSearchProvider() {
        return getConfig().getBoolean("search.elastic.enabled");
    }

    /**
     * If email mock is enabled, mails will be printed in console instead of sending
     * @return
     */
    public boolean isEmailMock() {
        return getConfig().getBoolean("smtp.mock");
    }

    public String getEmailSmtpHost() {
        return getConfig().getString("smtp.host");
    }

    public int getEmailSmtpPort() {
        return getConfig().hasPath("smtp.port") ? getConfig().getInt("smtp.port") : DEFAULT_SMTP_PORT;
    }

    public Optional<String> getEmailSmtpUser() {
        return getOptionalString("smtp.user");
    }

    public Optional<String> getEmailSmtpPassword() {
        return getOptionalString("smtp.password");
    }

    public String getEmailSender() {
        return getConfig().getString("smtp.sender");
    }

    public boolean getEmailSmtpTlsEnabled() {
        return getConfig().getBoolean("smtp.tls.enabled");
    }

    public boolean getEmailSmtpTlsRequired() {
        return getConfig().getBoolean("smtp.tls.required");
    }

    public int getReviewSubmissionsCount() {
        return getConfig().getInt("reviewblock.reviewSubmissionsCount");
    }

    /**
     * Get the list of configured backend languages (cms.backed.languages)
     * If not set, returns the general application.langs setting.
     */
    public List<String> backendLanguages() {
        // Get current site instance...
        Sites.Site currentSite = CMS.getSites().current();
        // And return its specific available backend languages
        return currentSite.langConfig.backendLanguageList;
    }

    /**
     * Get the list of configured cms frontend languages (cms.frontend.languages)
     * If not set, returns the general application.langs setting.
     */
    public List<String> frontendLanguages() {
        // Get current site instance...
        Sites.Site currentSite = CMS.getSites().current();
        // And return its specific available frontend languages
        return currentSite.langConfig.frontendLanguageList;
    }

    public List<String> allLanguages() {
        return Stream.of(
                frontendLanguages(),
                backendLanguages()
            )
            .flatMap(Collection::stream)
            .collect(Collectors.toList());
    }

    public boolean isCspEnabled() {
        return getConfig().getBoolean("csp.enabled");
    }

    public String cspResponseHeader() {
        return getConfig().getString("csp.responseHeader");
    }

    public Optional<String> cspAdditionalResponseHeader() {
        try {
            return Optional.of(getConfig().getString("csp.responseHeader_additional"));
        } catch(ConfigException.Missing e) {
            logger.warn("addition csp responseHeader is not defined, only using default csp-headers from CMS");
            return Optional.empty();
        }
    }

    /**
     * Get the list of additional scripts which should be loaded in layout-template of play-cms (Admin-Area)
     */
    public List<String> additionalScriptUrls() {
        return getConfig().getStringList("layout.additionalScripts");
    }

    /**
     * Get the list of additional scripts which should be loaded in layout-template of play-cms (Admin-Area)
     */
    public List<String> additionalStyleUrls() {
        return getConfig().getStringList("layout.additionalStyles");
    }

    private Optional<String> getOptionalString(String path) {
        return getConfig().hasPath(path)
                ? Optional.of(getConfig().getString(path)).filter(StringUtils::isNotBlank)
                : Optional.empty();
    }

}
