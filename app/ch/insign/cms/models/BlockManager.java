/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Block manager class.
 * @author bachi
 */
public class BlockManager  {
	private final static Logger logger = LoggerFactory.getLogger(BlockManager.class);

    private List<Class<? extends AbstractBlock>> blocks = new ArrayList<>();

    private Config contextConfig;


    public BlockManager() {
        try {
            contextConfig = ConfigFactory.load().getConfig("cms.context");
        } catch (Exception e) {
            logger.warn("Blockmanager: No cms.context settings found");
        }
    }

    /**
     * Flush the configuration
     */
    public void flush() {
        blocks.clear();
    }

    /**
     * Register your custom block classes.
     * @param blockClass
     */
    public void register(Class<? extends AbstractBlock>... blockClass) {
        for (int i = 0; i < blockClass.length; i++) {
            blocks.add(blockClass[i]);
            logger.debug("Registering block " + blockClass[i]);
        }
    }

    /**
     * Get the registered blocks, either all, or a filtered list containing those matching the selected types
     * @param type list of types to filter for.
     * @return
     */
    public List<Class<? extends AbstractBlock>> getBlocks(AbstractBlock.BlockType... type) {
        if (type.length == 0) return blocks;

        List<AbstractBlock.BlockType> typeList = Arrays.asList(type);
        List<Class<? extends AbstractBlock>> results = new ArrayList<>();
        for (Class<? extends AbstractBlock> clazz : blocks) {
            try {
                if (typeList.contains(clazz.newInstance().getType())) {
                    results.add(clazz);
                }
            } catch (Exception e) {
                logger.error("Failing getting registered block: " + clazz.getCanonicalName(), e);
                e.printStackTrace();
            }
        }
        return results;
    }

    /**
     * Get the list of allowed subblocks that can be added to the given block type and in the given context.
     * (e.g. 'Block in leftCol context')
     *
     * Also checks configuration values for specific settings, e.g.:
     *
     * cms.context.backend.allowedSubblocks = [BackendListBlock, ContentPage]
     * - or -
     * cms.context.frontend.excludedSubblocks = [BackendListBlock]
     *
     * @param type the AbstractBlock.BlockType of the parent
     * @param context the template context, e.g backend, frontend, col1 etc.
     * @return the list of possible / allowed subblock classes
     */
    public List<Class<? extends AbstractBlock>> getAllowedSubblocks(AbstractBlock.BlockType type, String context) {
        List<Class<? extends AbstractBlock>> result = new ArrayList<>();
        List<String> allowedSubblocks = new ArrayList<>();
        List<String> excludedSubblocks = new ArrayList<>();

        if (contextConfig != null && context != null && !context.isEmpty()) {
            try {
                allowedSubblocks = contextConfig.getStringList(context + ".allowedSubblocks");
            } catch (Exception e) {}

            try {
                excludedSubblocks = contextConfig.getStringList(context + ".excludedSubblocks");
            } catch (Exception e) {}
        }

        for (Class<? extends AbstractBlock> block : getBlocks(type)) {
            String blockName = block.getSimpleName();
            if (allowedSubblocks.size() > 0 && !allowedSubblocks.contains(blockName)) {
                continue;
            }

            if (excludedSubblocks.contains(blockName)) {
                continue;
            }

            result.add(block);
        }

        return result;
    }
}
