/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.blocks.backendlinkblock;

import ch.insign.cms.models.AbstractBlock;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import ch.insign.cms.blocks.linkblock.LinkBlock;
import ch.insign.cms.models.BlockFinder;
import ch.insign.cms.models.PageBlock;
import play.twirl.api.Html;
import play.data.Form;
import play.mvc.Controller;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

/**
 * Links used in the admin backend. They contain an additional icon.
 */
//@SecuredEntity(name = "BackendLinkBlock", actions = {"read", "modify"})
@Entity
@Table(name = "cms_backend_link_block")
@DiscriminatorValue("BackendLinkBlock")
public class BackendLinkBlock extends LinkBlock implements BackendMenuItem {
	private final static Logger logger = LoggerFactory.getLogger(BackendLinkBlock.class);

    public static BlockFinder<BackendLinkBlock> find = new BlockFinder<>(BackendLinkBlock.class);

    private String linkIcon;

    public String getLinkIcon() {
        return linkIcon;
    }

    public void setLinkIcon(String linkIcon) {
        this.linkIcon = linkIcon;
    }

    @Override
    public Html editForm(Form editForm) {
        return ch.insign.cms.blocks.backendlinkblock.html.backendLinkBlockEdit.render(this, (Form<BackendLinkBlock>) editForm, Controller.request().getQueryString("backURL"), null);
    }

    @Override
    public List<Class<? extends AbstractBlock>> getAllowedSubBlocks() {
        List<Class<? extends AbstractBlock>> blocks = new ArrayList<>();
        return blocks;
    }
}
