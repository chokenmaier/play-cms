/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.blocks.reviewblock.models;

import ch.insign.cms.blocks.reviewblock.controllers.ReviewFrontendController;
import ch.insign.commons.db.Model;
import ch.insign.playauth.PlayAuth;
import ch.insign.playauth.party.Party;
import play.libs.F;

import javax.annotation.Nullable;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "cms_review_submission", indexes = {
		@Index(name = "email", columnList = "email"),
		@Index(name = "isApproved", columnList = "isApproved"),
		@Index(name = "createdOn", columnList = "createdOn")
})
@NamedQueries(value = {
		@NamedQuery(
				name = "ReviewSubmission.byReviewAndUid",
				query = "SELECT s FROM ReviewSubmission s WHERE s.uid = :uid AND s.review = :review"
		),
		@NamedQuery(
				name = "ReviewSubmission.byUserKeywords",
				query = "SELECT DISTINCT s FROM ReviewSubmission s INNER JOIN ReviewBlock b ON s.review.id = b.id WHERE "
						+ "(s.id LIKE :termId OR b.id LIKE :termId OR b.targetId LIKE :term OR s.email LIKE :term) "
						+ "ORDER BY s.isApproved ASC, s.createdOn DESC"
		),
		@NamedQuery(
				name = "ReviewSubmission.byUserKeywords.count",
				query = "SELECT COUNT(s) FROM ReviewSubmission s WHERE s in ("
						+ "SELECT s FROM ReviewSubmission s INNER JOIN ReviewBlock b ON s.review.id = b.id WHERE "
						+ "(s.id LIKE :termId OR b.id LIKE :termId OR b.targetId LIKE :term OR s.email LIKE :term))"
		),
		@NamedQuery(
				name = "ReviewSubmission.byReviewAndApprovedSortedByRating",
				query = "SELECT s FROM ReviewSubmission s WHERE s.review = :review and s.isApproved = true "
						+ "ORDER BY s.answerRating DESC, s.createdOn DESC"
		),
		@NamedQuery(
				name = "ReviewSubmission.byReviewAndApprovedSortedByCreation",
				query = "SELECT s FROM ReviewSubmission s WHERE s.review = :review and s.isApproved = true "
						+ "ORDER BY s.createdOn DESC"
		),
		@NamedQuery(
				name = "ReviewSubmission.byReviewAndApproved.count",
				query = "SELECT COUNT(s) FROM ReviewSubmission s WHERE s.review = :review and s.isApproved = true "
		)
})
public class ReviewSubmission extends Model {
	public static ReviewSubmissionFinder<ReviewSubmission> find = new ReviewSubmissionFinder<>(ReviewSubmission.class);

	@ManyToOne
	protected ReviewBlock review;

	@Lob
	protected String answer;

	@Lob
	protected String adminAnswer;

	protected Date adminAnswerDate;

	protected String uid;

	protected String email;

	protected String displayName;

	protected Integer rating;

	protected Integer answerRating = 0;

	@ElementCollection
	@MapKeyColumn(name="USER_ID")
	@Column(name="RATING")
	@CollectionTable(name="cms_review_submission_thumbs_up_down", joinColumns=@JoinColumn(name="REVIEW_SUBMISSION_ID"))
	private Map<String, Integer> reviewRatings = new HashMap<>();

	protected Date createdOn;

	protected boolean isApproved = false;

	@PrePersist
	public void onPrePersist() {
		createdOn = new Date();
	}

	public ReviewBlock getReview() {
		return review;
	}

	public void setReview(ReviewBlock review) {
		this.review = review;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getAdminAnswer() {
		return adminAnswer;
	}

	public void setAdminAnswer(String adminAnswer) {
		this.adminAnswer = adminAnswer;
	}

	public Date getAdminAnswerDate() {
		return adminAnswerDate;
	}

	public void setAdminAnswerDate(Date adminAnswerDate) {
		this.adminAnswerDate = adminAnswerDate;
	}

	public boolean isApproved() {
		return isApproved;
	}

	public void approve() {
		if (!isApproved) {
			isApproved = true;
			getReview().refreshStatistic();
			//submission.getReview().includeSubmissionRating(submission);
			getReview().getTarget()
					.ifPresent(block -> block.markModified());
		}
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Integer getAnswerRating() {
		return answerRating;
	}

	public void setAnswerRating(Integer answerRating) {
		this.answerRating = answerRating;
	}

	public Optional<Party> getParty() {
		return Optional.ofNullable(PlayAuth.getPartyManager().find(uid));
	}

	public void addRating(String uid, Integer rating) {
		reviewRatings.put(uid, rating);
		answerRating += rating;
	}

	public void removeRating(String uid) {
		Integer rating = reviewRatings.get(uid);
		if (null != rating) {
			answerRating -= rating;
			reviewRatings.remove(uid);
		}
	}

	public boolean hasRating(String uid) {
		return reviewRatings.containsKey(uid);
	}

	public Integer getAnswerRating(String uid) {
		return reviewRatings.get(uid);
	}

	public boolean hasPositiveRating(String uid) {
		Integer rating = reviewRatings.get(uid);
		if (null != rating && rating > 0) {
			return true;
		}
		return false;
	}

	public boolean hasNegativeRating(String uid) {
		Integer rating = reviewRatings.get(uid);
		if (null != rating && rating < 0) {
			return true;
		}
		return false;
	}

	public F.Tuple<String, String> getSplitedAnswer(int length) {
		return getSplitedAnswer(length, "");
	}

	public F.Tuple<String, String> getSplitedAnswer(int length, String separator) {
		if(answer == null || answer.trim().isEmpty()){
			return new F.Tuple<>("", "");
		}
		String firstPart = answer, secondPart = "";
		StringBuffer sb = new StringBuffer(answer);

		int actualLength = length - separator.length();
		if(sb.length() > actualLength && sb.indexOf(" ", actualLength) > 0) {
			int endIndex = sb.indexOf(" ", actualLength);
			firstPart = sb.insert(endIndex, separator).substring(0, endIndex + separator.length());
			secondPart = answer.substring(firstPart.length() - separator.length());
		}

		return new F.Tuple<>(firstPart, secondPart);
	}

	public static class ReviewSubmissionFinder<T extends ReviewSubmission> extends Finder<T> {
		public ReviewSubmissionFinder(Class<T> entity) {
			super(entity);
		}

		@Nullable
		public T byReviewAndUid(ReviewBlock reviewBlock, String uid) {
			try {
				return jpaApi.em().createNamedQuery("ReviewSubmission.byReviewAndUid", getEntityClass())
						.setParameter("review", reviewBlock)
						.setParameter("uid", uid)
						.getSingleResult();
			} catch (NoResultException e) {
				return null;
			}
		}

		public F.Tuple<TypedQuery<T>, Long> byUserKeywords(String term) {
			return new F.Tuple<>(byUserKeywordsResult(term),
					byUserKeywordsCount(term));
		}

		public F.Tuple<TypedQuery<T>, Long> byReview(ReviewBlock reviewBlock, ReviewFrontendController.Sorting sortedBy) {
			return new F.Tuple<>(byReviewAndApproved(reviewBlock, sortedBy),
					byReviewAndApprovedCount(reviewBlock));
		}

		public TypedQuery<T> byReviewAndApproved(ReviewBlock reviewBlock, ReviewFrontendController.Sorting sortedBy) {
			String namedQuery = ReviewFrontendController.Sorting.THUMBS_UP.equals(sortedBy)
					? "ReviewSubmission.byReviewAndApprovedSortedByRating"
					: "ReviewSubmission.byReviewAndApprovedSortedByCreation";

			return jpaApi.em().createNamedQuery(namedQuery, getEntityClass())
					.setParameter("review", reviewBlock);
		}

		public Long byReviewAndApprovedCount(ReviewBlock reviewBlock) {
			try {
				return jpaApi.em().createNamedQuery("ReviewSubmission.byReviewAndApproved.count", Long.class)
						.setParameter("review", reviewBlock)
						.getSingleResult();
			} catch (NoResultException e) {
				return 0L;
			}
		}

		private TypedQuery<T> byUserKeywordsResult(String term) {
			Long termId = -1L;

			try {
				termId = Long.parseLong(term);
			} catch (Exception e) {
				// Null or not a numerical value, so just ignore it
			}

			return jpaApi.em().createNamedQuery("ReviewSubmission.byUserKeywords", getEntityClass())
					.setParameter("termId", termId)
					.setParameter("term", "%" + term.trim() + "%");
		}

		private Long byUserKeywordsCount(String term) {
			Long termId = -1L;

			try {
				termId = Long.parseLong(term);
			} catch (Exception e) {
				// Null or not a numerical value, so just ignore it
			}

			try {
				return jpaApi.em().createNamedQuery("ReviewSubmission.byUserKeywords.count", Long.class)
						.setParameter("termId", termId)
						.setParameter("term", "%" + term.trim() + "%")
						.getSingleResult();
			} catch (NoResultException e) {
				return 0L;
			}
		}

	}
}
