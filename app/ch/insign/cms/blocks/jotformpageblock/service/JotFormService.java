/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.blocks.jotformpageblock.service;

import ch.insign.cms.blocks.jotformpageblock.JotFormPageBlock;
import ch.insign.cms.blocks.jotformpageblock.model.JotForm;
import ch.insign.cms.blocks.jotformpageblock.model.JotFormSubmissions;
import ch.insign.cms.blocks.jotformpageblock.view.html.detailsTabTemplate;
import ch.insign.commons.util.Configuration;
import ch.insign.playauth.PlayAuth;
import ch.insign.playauth.party.address.EmailAddress;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.inject.Singleton;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.Play;
import play.db.jpa.JPA;
import play.i18n.Messages;
import play.libs.Akka;
import play.libs.Json;
import play.twirl.api.Html;
import scala.concurrent.duration.Duration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Singleton
public class JotFormService {

    private final static JotFormApi jotFormApi = new JotFormApi(Configuration.getOrElse("jotform.api.key", ""));
    private final static Logger logger = LoggerFactory.getLogger(JotFormService.class);
    public static final String NUMBER_OF_RESULT = "100";
    public static final String JOT_FORM_CREATE_DATE = "created_at";
    public static final String START_RESULT_FROM = "0";
    public static final String DATATABLE_HEADER = "sTitle";
    public static final String JOT_FORM_KEY_CONTENT = "content";
    public static final String JOT_FORM_KEY_TEXT = "text";
    public static final String JOT_FORM_KEY_OPTIONS = "options";
    public static final String JOT_FORM_KEY_ANSWERS = "answers";
    public static final String JOT_FORM_KEY_PRETTY_FORMAT = "prettyFormat";
    public static final String JOT_FORM_KEY_EMAIL_TYPE = "control_email";
    public static final String JOT_FORM_KEY_ANSWER = "answer";
    public static final String JOT_FORM_KEY_TITLE = "title";
    public static final String JOT_FORM_KEY_TYPE = "type";
    public static final String JOT_FORM_ID_KEY = "form_id";
    public static final String JOT_FORM_UPDATE_DATE = "updated_at";
    public static final int VALID_RESPONCE_CODE = 200;

    public JotFormService() {
    }

    /**
     * @return All user forms from api
     */
    public JSONObject getUserForms() {
        HashMap<String, String> filter = new HashMap<>();
        filter.put("status", "ENABLED");
        return jotFormApi.getForms(START_RESULT_FROM, NUMBER_OF_RESULT, filter, JOT_FORM_CREATE_DATE);
    }

    /**
     * @param pageId - JotFormPageBlock id
     * @return form api all titles for form submission
     */
    public ArrayNode getFormSubmissionsHeaders(String pageId, String lang) {

        ArrayNode headers = Json.newArray();

        headers.add(Json.newObject().put(DATATABLE_HEADER, "#"));
        headers.add(Json.newObject().put(DATATABLE_HEADER, Messages.get("jotform.backend.table.head.date")));
        headers.add(Json.newObject().put(DATATABLE_HEADER, Messages.get("jotform.backend.table.head.submission")));

        JotFormPageBlock jotFormPageBlock = JotFormPageBlock.find.byId(pageId);

        if (jotFormPageBlock == null || StringUtils.isBlank(jotFormPageBlock.getJotFormId().get(lang))) {
            return headers;
        }

        JSONObject formQuestions = getQuestions(Long.valueOf(jotFormPageBlock.getJotFormId().get(lang)));


        if (formQuestions == null || formQuestions.length() < 0) {
            return headers;
        }

        addSubmissionsHeadersFromQuestions(formQuestions, headers);

        return headers;
    }

    /**
     * Adds webhook for synchronization submissions
     *
     * @param formId jotForm Id
     */
    public void setupJotFormSubmissionsWebHook(long formId) {
        List<String> jotFormUrls = new ArrayList<>();

        JSONObject webhooksResponse = jotFormApi.getFormWebhooks(formId);

        JSONObject content = getJsonContent(webhooksResponse);

        String jotFormSubmissionsWebHookUrl = getJotFormSubmissionsWebHookUrl(formId);

        if (!validResponseCode(webhooksResponse)) {
            logger.debug("JotForm setupJotFormSubmissionsWebHook response errors");
            return;
        }

        if (jotFormSubmissionsWebHookUrl == null) {
            logger.debug("JotForm setupJotFormSubmissionsWebHook jotFormSubmissionsWebHookUrl == null");
            return;
        }

        if (content == null) {
            addWebHook(formId, jotFormSubmissionsWebHookUrl);
            logger.debug("JotForm setupJotFormSubmissionsWebHook addWebHook = " + jotFormSubmissionsWebHookUrl);
            return;
        }

        for (String key : content.keySet()) {
            jotFormUrls.add((String)content.get(key));
        }

        if (!jotFormUrls.contains(jotFormSubmissionsWebHookUrl)) {
            logger.info("JotForm setupJotFormSubmissionsWebHook addWebHook = " + jotFormSubmissionsWebHookUrl);
            addWebHook(formId, jotFormSubmissionsWebHookUrl);
        }
    }

    private String getJotFormSubmissionsWebHookUrl(Long id) {
        final String baseUrl = Play.application().configuration().getString("cms.baseUrl");
        return baseUrl + ch.insign.cms.blocks.jotformpageblock.controller.routes.JotFormController.updateFormDataSubmission(id).url();
    }

    /**
     * @param object
     * @return Сhecks the type of INPUT is available for use in CMS
     */
    public static boolean isQuestionsType(JSONObject object) {
        String type;

        try {
            type = String.valueOf(object.get("type"));
        } catch (JSONException e) {
            logger.warn("JotForm (isQuestionsType) get type exception");
            return false;
        }

        if (type != null && getAllowableQuestions().contains(type)) {
            return true;
        }

        return false;
    }

    /**
     * @param id - JotFormApi id
     * @return Gets all form questions
     */
    public static JSONObject getQuestions(Long id) {
        JSONObject submissions = jotFormApi.getFormQuestions(id);

        if (!validResponseCode(submissions) || submissions == null) {
            return null;
        }

        return getJsonContent(submissions);
    }


    private static boolean isEmailUserTag(JSONObject answers) {

        if (answers.has(JOT_FORM_KEY_TYPE) && answers.get(JOT_FORM_KEY_TYPE).equals(JOT_FORM_KEY_EMAIL_TYPE)) {
            return true;
        }

        return false;
    }

    /**
     * @param submission
     * @return values for submission
     */
    public static JSONObject getAnswers(JSONObject submission) {
        if (!(submission).has(JOT_FORM_KEY_ANSWERS)
                || (submission).get(JOT_FORM_KEY_ANSWERS) == null
                || !((submission).get(JOT_FORM_KEY_ANSWERS) instanceof JSONObject)) {
            return null;
        }
        return (JSONObject) submission.get(JOT_FORM_KEY_ANSWERS);
    }

    /**
     * @param pageId JotFormPageBlock id
     * @param lang Current language
     * @return All the values jot form submissions from api
     */
    public ArrayNode getFormSubmissionsData(String pageId, String lang) {
        ArrayNode entries = Json.newArray();
        JotFormPageBlock jotFormPageBlock = JotFormPageBlock.find.byId(pageId);

        if (jotFormPageBlock == null || StringUtils.isBlank(jotFormPageBlock.getJotFormId().get(lang))) {
            return entries;
        }

        Long formId = Long.valueOf(jotFormPageBlock.getJotFormId().get(lang));

        JSONObject submissions = jotFormApi.getFormSubmissions(formId, "", "", null, JOT_FORM_CREATE_DATE);

        JSONObject formQuestions = getQuestions(formId);

        JSONArray content = getJsonArrayContent(submissions);

        if (!validResponseCode(submissions) || content == null || content.length() < 0 || formQuestions == null) {
            logger.warn("JotForm getFormSubmissionsData response errors");
            return entries;
        }

        for (int i = 0; i < content.length(); i++) {
            JSONObject answers = getAnswers((JSONObject) content.get(i));

            if (answers != null) {
                ArrayNode links = Json.newArray();
                String submissionId = String.valueOf(((JSONObject) content.get(i)).get("id"));

                links.add(i);

                try {
                    links.add(String.valueOf(((JSONObject) content.get(i)).get(JOT_FORM_CREATE_DATE)));
                } catch (JSONException e) {
                    links.add("");
                    logger.error("JotForm  get create date exception");
                }
                links.add(submissionId);
                addSubmissionsDataFromAnswers(formQuestions.keySet(), answers, links, formId, jotFormPageBlock.isSyncEmail(), Long.valueOf(submissionId));
                entries.add(links);
            }
        }
        return entries;
    }

    /**
     * @param jsonObject
     * @return Check the answer is correct from api
     */
    public static boolean validResponseCode(JSONObject jsonObject) {

        if (jsonObject.get("responseCode") != null && jsonObject.get("responseCode").equals(VALID_RESPONCE_CODE)) {
            return true;
        }

        logger.warn("JotForm response not valid");
        return false;
    }

    public boolean exist(Long id) {
        if (JotForm.find.byUid(id) != null) {
            return true;
        }
        return false;
    }

    public void create(JSONObject jsonObject) {
        Long id = getId(jsonObject);
        Date createDate = getJotFormCreateDate(jsonObject);
        String title = getJotFormTitle(jsonObject);

        if (id != null) {
            JotForm jotForm = new JotForm(id, createDate, title, Configuration.getOrElse("jotform.api.key", ""));
            jotForm.save();
        } else {
            logger.error("JotForm, get form id from content error");
        }
    }

    public static JSONObject getAPIform(Long id) {
        return jotFormApi.getForm(id);
    }

    /**
     * @return All New Submissions for JotForm in CMS
     */
    public static JSONObject getJotFormNewSubmissions(long id) {
        HashMap<String, String> filter = new HashMap<>();
        filter.put("status", "ACTIVE");
        filter.put("new", "1");

        JSONObject newSubmissions = jotFormApi.getFormSubmissions(id, "", "", filter, JOT_FORM_CREATE_DATE);

        if (!validResponseCode(newSubmissions) || newSubmissions == null) {
            logger.warn("JotForm error in getJotFormNewSubmissions method");
            return null;
        }
        return newSubmissions;
    }

    /**
     * @return All New Submissions for all JotForm in CMS
     */
    public static JSONObject getJotFormsActiveSubmissions() {
        HashMap<String, String> filter = new HashMap<>();
        filter.put("status", "ACTIVE");

        JSONObject newSubmissions = jotFormApi.getSubmissions("", "", filter, JOT_FORM_CREATE_DATE);

        if (!validResponseCode(newSubmissions) || newSubmissions == null) {
            logger.warn("JotForm error in getJotFormNewSubmissions method");
            return null;
        }
        return newSubmissions;
    }

    public void syncJotFormSubmissions(Long id) {
        if (id != null && id > 0) {
            Akka.system().scheduler().scheduleOnce(
                    Duration.create(1, TimeUnit.SECONDS),
                    new Runnable() {
                        public void run() {
                            JPA.withTransaction(() -> {
                                logger.debug("SyncSubmissions START, form id = " + id);
                                JSONObject jotFormNewSubmission = getJotFormNewSubmissions(id);

                                if (jotFormNewSubmission == null) {
                                    logger.error("JotForm error in updateJotFormUser method, getJotFormNewSubmissions");
                                    return;
                                }

                                JSONArray content = getJsonArrayContent(jotFormNewSubmission);

                                if (content == null || content.length() < 0) {
                                    logger.debug("JotForm error in SyncSubmissions method, empty");
                                    return;
                                }

                                addJotFormSubmissions(content);
                                logger.debug("SyncSubmissions END, form id = " + id);
                            });
                        }
                    },
                    Akka.system().dispatcher()
            );
        } else {
            logger.error("Error in syncJotFormSubmissions");
        }
    }

    public void syncAllJotFormsSubmissions() {
        JSONObject jotFormNewSubmissions = getJotFormsActiveSubmissions();

        if (jotFormNewSubmissions == null) {
            logger.debug("JotForm error in updateJotFormUser method, getJotFormNewSubmissions");
            return;
        }

        JSONArray content = getJsonArrayContent(jotFormNewSubmissions);

        if (content == null || content.length() < 0) {
            logger.debug("JotForm error in SyncSubmissions method, empty");
            return;
        }

        addJotFormSubmissions(content);
    }


    /**
     * @param email
     * @return Checks whether the user exists in DB
     */
    private static boolean isUserExists(String email) {
        if (PlayAuth.getPartyManager().findOneByPrincipal(new EmailAddress(email)) != null) {
            return true;
        }

        return false;
    }

    private static boolean isJotFormLogNeedStore(String email, JSONObject answer) {
        if (isEmailUserTag(answer) && isUserExists(email)) {
            return true;
        }
        return false;
    }

    private static void addJotFormSubmissions(JSONArray content) {
        HashMap<String, String> formValue = new HashMap<>();


        for (int i = 0; i < content.length(); i++) {
            Long formId = Long.valueOf((String) ((JSONObject) content.get(i)).get(JOT_FORM_ID_KEY));

            JSONObject formQuestions = getQuestions(formId);

            boolean isSubmissionSaveInDB = false;

            List<JotFormPageBlock> blocks = JotFormPageBlock.find.byApiJotFormId(formId).getResultList();

            Long submissionId = null;
            try {
                submissionId = Long.valueOf((String) ((JSONObject) content.get(i)).get("id"));
            } catch (Exception e) {
                logger.debug("JotForm error in addJotFormSubmissions id parse error");
            }

            if (submissionId  == null|| !JotFormSubmissions.find.findBySubmissionId(submissionId).isEmpty()) {
                continue;
            }

            for (JotFormPageBlock block: blocks) {
                if (block != null
                        && block.isSyncEmail()
                        && block.isVisible()
                        && !block.isTrashed()
                        && !block.isDraft()
                        && formQuestions != null) {

                    JotFormSubmissions jotFormUserSubmissions = new JotFormSubmissions();

                    jotFormUserSubmissions.setFormId(formId);
                    jotFormUserSubmissions.setSubmissionId(submissionId);
                    jotFormUserSubmissions.setFormTitle(getJotFormTitle(getAPIform(formId)));

                    JSONObject answers = getAnswers((JSONObject) content.get(i));
                    if (answers != null) {
                        for (String key : formQuestions.keySet()) {
                            if (answers.has(key)) {
                                if (isQuestionsType((JSONObject) answers.get(key))
                                        && (((JSONObject) answers.get(key)).has(JOT_FORM_KEY_TEXT) || ((JSONObject) answers.get(key)).has(JOT_FORM_KEY_OPTIONS))) {

                                    String title = "-";
                                    try {
                                        if (((JSONObject) answers.get(key)).has(JOT_FORM_KEY_TEXT)) {
                                            title = String.valueOf(((JSONObject) answers.get(key)).get(JOT_FORM_KEY_TEXT));
                                        } else {
                                            title = String.valueOf(((JSONObject) answers.get(key)).get(JOT_FORM_KEY_OPTIONS));
                                        }
                                    } catch (JSONException e) {
                                        logger.debug("JotForm addJotFormSubmissions method, parse title JSONException");
                                    }

                                    String value = "-";
                                    try {
                                        if (((JSONObject) answers.get(key)).has(JOT_FORM_KEY_PRETTY_FORMAT)) {
                                            String text = "";

                                            text = String.valueOf(((JSONObject) answers.get(key)).get(JOT_FORM_KEY_PRETTY_FORMAT));
                                            value = StringUtils.isNotBlank(text) ? text : "-";

                                        } else {
                                            String options = "";
                                                options = String.valueOf(((JSONObject) answers.get(key)).get(JOT_FORM_KEY_ANSWER));
                                                value = StringUtils.isNotBlank(options) ? options : "-";
                                        }
                                    } catch (JSONException e) {
                                        logger.debug("JotForm addJotFormSubmissions method, parse value JSONException");
                                    }

                                    try {

                                        if (isJotFormLogNeedStore(value, (JSONObject) answers.get(key))) {
                                            jotFormUserSubmissions.setUserEmail(value);
                                            isSubmissionSaveInDB = true;
                                        }

                                    } catch (JSONException e) {
                                        logger.debug("JotForm isJotFormLogNeedStore method, parse value JSONException");
                                    }
                                    formValue.put(title, value);
                                }
                            }
                        }

                        jotFormUserSubmissions.setFormValues(formValue);

                        if (isSubmissionSaveInDB) {
                            jotFormUserSubmissions.save();
                        }
                    }
                }
            }
        }
    }

    public JotForm get(Long id) {
        return JotForm.find.byUid(id);
    }

    /**
     * @param content
     * @return View content on the page need to override in projects
     */
    public Html render(Html content, JotFormPageBlock block) {
        return ch.insign.cms.blocks.jotformpageblock.view.html.main.render(content, block);
    }

    /**
     * @param email - user emails
     * @return View link for user tab need to override in projects
     */
    public Html detailsTabTemplate(String email) {
        return detailsTabTemplate.render(email);
    }

    /**
     * @param id  JotForm Id
     * @param url WebHook url
     *            Ability to add WebHook for jot form
     */
    public static void addWebHook(Long id, String url) {
        JSONObject responce = jotFormApi.createFormWebhook(id, url);
        if (!validResponseCode(responce)) {
            logger.error("JotForm error in addWebHook method");
        }
    }

    private void addSubmissionsDataFromAnswers(Set<String> keys, JSONObject answers, ArrayNode links, Long id, boolean syncEmail, Long submissionId) {
        for (String key : keys) {
            if(answers.has(key)) {
                if (isQuestionsType((JSONObject) answers.get(key))
                        && ((JSONObject) answers.get(key)).has(JOT_FORM_KEY_PRETTY_FORMAT) || ((JSONObject) answers.get(key)).has(JOT_FORM_KEY_ANSWER)) {
                    if (((JSONObject) answers.get(key)).has(JOT_FORM_KEY_PRETTY_FORMAT)) {

                        String text = String.valueOf(((JSONObject) answers.get(key)).get(JOT_FORM_KEY_PRETTY_FORMAT));
                        links.add(StringUtils.isNotBlank(text) ? text : "-");

                    } else {
                        String options = String.valueOf(((JSONObject) answers.get(key)).get(JOT_FORM_KEY_ANSWER));

                        if (JotFormSubmissions.find.findByUserEmailAndSubmissionId(options, submissionId) != null) {
                            links.add(detailsTabTemplate(options).body());
                        } else {
                            links.add(StringUtils.isNotBlank(options) ? options : "-");
                        }
                    }
                } else {
                    links.add("-");
                }
            }
        }
    }

    private void addSubmissionsHeadersFromQuestions(JSONObject formQuestions, ArrayNode headers) {
        if (!formQuestions.keySet().isEmpty()) {
            for (String key : formQuestions.keySet()) {
                if (isQuestionsType((JSONObject) formQuestions.get(key))
                        && (((JSONObject) formQuestions.get(key)).has(JOT_FORM_KEY_TEXT) || ((JSONObject) formQuestions.get(key)).has(JOT_FORM_KEY_OPTIONS))) {

                    if (((JSONObject) formQuestions.get(key)).has(JOT_FORM_KEY_TEXT)) {

                        String text = String.valueOf((((JSONObject) formQuestions.get(key)).get(JOT_FORM_KEY_TEXT)));
                        headers.add(Json.newObject().put(DATATABLE_HEADER, StringUtils.isNotBlank(text) ? text : "-"));

                    } else {

                        String options = String.valueOf((((JSONObject) formQuestions.get(key)).get(JOT_FORM_KEY_OPTIONS)));
                        headers.add(Json.newObject().put(DATATABLE_HEADER, StringUtils.isNotBlank(options) ? options : "-"));

                    }
                }
            }
        }
    }

    private static Long getId(JSONObject jsonObject) {
        JSONObject content = getJsonContent(jsonObject);

        if (content != null && content.get("id") != null) {
            return Long.valueOf((String) content.get("id"));
        }

        return null;
    }

    private Date getJotFormCreateDate(JSONObject jsonObject) {
        JSONObject content = getJsonContent(jsonObject);

        if (content != null && content.get(JOT_FORM_CREATE_DATE) != null) {
            return stringToDate(content.get(JOT_FORM_CREATE_DATE).toString());
        }
        return null;
    }

    private static String getJotFormTitle(JSONObject jsonObject) {
        JSONObject content = getJsonContent(jsonObject);

        if (content != null && content.get(JOT_FORM_KEY_TITLE) != null) {
            return content.get(JOT_FORM_KEY_TITLE).toString();
        }
        return null;
    }

    private static Date stringToDate(String str) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = formatter.parse(str);
        } catch (ParseException e) {
            return null;
        }
        return date;
    }

    /**
     * @return Types jotForm INPUT possible  support in the CMS
     */
    private static ArrayList<String> getAllowableQuestions() {
        String[] questions = {
                "control_autocomp", "control_textbox", "control_email", "control_number", "control_textarea", "control_dropdown",
                "control_checkbox", "control_radio", "control_datetime", "control_time", "control_rating", "control_slider", "control_spinner",
                "control_range", "control_fullname", "control_grading", "control_birthdate", "control_phone", "control_address"
        };

        return new ArrayList<String>(Arrays.asList(questions));
    }

    private static JSONObject getJsonContent(JSONObject jsonObject) {
        try {
            return jsonObject.getJSONObject(JOT_FORM_KEY_CONTENT);
        } catch (JSONException e) {
            logger.debug("JotForm (JSONObject) get content exception");
            return null;
        }
    }

    private static JSONArray getJsonArrayContent(JSONObject jsonObject) {
        try {
            return jsonObject.getJSONArray(JOT_FORM_KEY_CONTENT);
        } catch (JSONException e) {
            logger.debug("JotForm (JSONArray) get content exception");
            return null;
        }
    }
}
