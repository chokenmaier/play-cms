/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.attributeset.model;

import ch.insign.commons.db.Model;

import javax.persistence.*;

/**
 * A class that represent a specific value of an attribute, that belongs (or is saved for) a specific entity.
 * Since different attributes need different values, this is an abstract class that needs a concrete implementation
 * for each attribute. For example, a date attribute needs a class that has a Date property; or a multiselect
 * attribute needs a class that has a List<Option> property. See {@link OptionValue} or {@link TextValue} for
 * reference implementations.
 */
@Entity
@Table(name="cms_eav_attribute_value")
@Inheritance(strategy= InheritanceType.JOINED)
@DiscriminatorColumn(name="value_type")
public abstract class Value extends Model {

    @ManyToOne
    private AttributeSetInstance instance;

    // ====== Getter and Setter ======

    public AttributeSetInstance getInstance() {
        return instance;
    }

    public void setInstance(AttributeSetInstance instance) {
        this.instance = instance;
    }
}
